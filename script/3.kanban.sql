CREATE TABLE public.kanban
(
    id SERIAL NOT NULL,
    nombre text COLLATE pg_catalog."default",
    fecha_reg date,
    fecha_mod date,
    CONSTRAINT "KANBAN_pkey" PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.kanban
    OWNER to postgres;