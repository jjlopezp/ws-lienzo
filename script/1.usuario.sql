--DROP TABLE public."USUARIO";

CREATE TABLE public.usuario
(
    id SERIAL NOT NULL,
    usuario text COLLATE pg_catalog."default",
    nombre text COLLATE pg_catalog."default",
    apellido_paterno text COLLATE pg_catalog."default",
    apellido_materno text COLLATE pg_catalog."default",
    email text COLLATE pg_catalog."default",
    contrasena text COLLATE pg_catalog."default",
    telefono text COLLATE pg_catalog."default",
    linkedin text COLLATE pg_catalog."default",
    estado integer,
    fecha_reg date,
    fecha_mod date,
    CONSTRAINT "USUARIO_pkey" PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.usuario
    OWNER to postgres;