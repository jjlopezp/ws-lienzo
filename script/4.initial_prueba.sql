insert into prueba_hipotesis(nombre, descripcion, estado)
values('Análisis de datos', 'Consiste en realizar una búsqueda rápida en internet con términos claves sobre lo que trata tu producto o servicio a brindar', 1);

insert into prueba_hipotesis(nombre, descripcion, estado)
values('Foro', 'Realizar consultas u ofrecer el servicio en foros, de esta manera se obtendrá un feedback rápido de las personas', 1);

insert into prueba_hipotesis(nombre, descripcion, estado)
values('Entrevista', 'Realizar entrevistas a los potenciales clientes para obtener información relevante.', 1);

insert into prueba_hipotesis(nombre, descripcion, estado)
values('Publicidad', 'Colocar anuncios en distintas páginas concurridas y capturar la cantidad de personas que ingresan a la publicidad, de esta manera se podrá medir la cantidad de usuarios interesados en el servicio.', 1);

insert into prueba_hipotesis(nombre, descripcion, estado)
values('Mago de Oz', 'Consiste en hacer manualmente las tareas que se realizarian de forma automática. El cliente debe crear que efectivamente es automático. Un ejemplo conocido fue el caso de la empresa zappos', 1);

insert into prueba_hipotesis(nombre, descripcion, estado)
values('Landing page', 'Es una página web con pocos elementos, enfocadas principalmente a un único producto o servicio. El objetivo de esta prueba es que los usuarios no se distraigan con elementos que no son parte del objetivo, de esta manera tener el seguimiento de la interacción que realizan con la página', 1);

insert into prueba_hipotesis(nombre, descripcion, estado)
values('Prueba A/B', 'Se realiza 2 versiones sobre un casuistica puntual para observar el comportamiento del usuario y obtener el feedback rápidamente', 1);


insert into prueba_hipotesis(nombre, descripcion, estado)
values('Producto mínimo viable', 'Es un prototipo muy cercano a lo que será el producto final, utilizado para ver la interacción real del usuario frente al producto parcial', 1);

insert into prueba_hipotesis(nombre, descripcion, estado)
values('Entrevista', 'Realizar entrevistas a los potenciales clientes para obtener información relevante.', 1);