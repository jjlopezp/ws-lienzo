CREATE TABLE public.proyecto
(
    id SERIAL NOT NULL,
    id_kanban integer NOT NULL,
    nombre text COLLATE pg_catalog."default",
    descripcion text COLLATE pg_catalog."default",
    fecha_reg date,
    fecha_mod date,
    CONSTRAINT "PROYECTO_pkey" PRIMARY KEY (id),
    CONSTRAINT id_kanban FOREIGN KEY (id_kanban)
        REFERENCES public.kanban (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.proyecto
    OWNER to postgres;