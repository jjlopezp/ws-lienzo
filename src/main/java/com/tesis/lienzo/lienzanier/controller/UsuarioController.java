package com.tesis.lienzo.lienzanier.controller;

import java.io.IOException;
import java.net.PasswordAuthentication;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tesis.lienzo.lienzanier.bean.Usuario;
import com.tesis.lienzo.lienzanier.service.UsuarioService;

@RestController
@CrossOrigin
@RequestMapping("/usuario")
public class UsuarioController {

	@Autowired
	UsuarioService usuarioService;
	
	@Autowired
	EmailController emailController;
	
	@GetMapping("/getAll")
	List<Usuario> getAllUsuarios() {
		return usuarioService.getAllUsuarios();
	}
	
	@GetMapping("/validateUsuarioRegistrado")
	Boolean validateUsuarioRegistrado(@RequestParam(value="usuario") String usuario) {
		return usuarioService.validateUsuarioRegistrado(usuario);
	}
	
	@GetMapping("/validateEmailRegistrado")
	Boolean validateEmailRegistrado(@RequestParam(value="email") String usuario) {
		return usuarioService.validateEmailRegistrado(usuario);
	}
	
	@PostMapping("/save")
	String saveUsuario(@RequestBody Usuario usuario) {
		return usuarioService.save(usuario);
	}
	
	@PostMapping("/login")
	Usuario login(@RequestBody Usuario usuario) {
		return usuarioService.login(usuario);
	}
	
	@GetMapping("/getByEmail")
	List<Usuario> findByEmail(@RequestParam (value="email") String email,
							  @RequestParam (value="current") String usuarioActual) {
		return usuarioService.findByEmail(email, usuarioActual);
	}
	
	@PostMapping("/loginG")
	Usuario loginG(@RequestBody Usuario usuario) {
		return usuarioService.loginG(usuario);
	}
	
	@PostMapping("/registerShare")
	Usuario registerShare(@RequestBody Usuario usuario) throws AddressException, MessagingException, IOException {
		usuario.setRol(2);
		//Enviar correo
		emailController.sendmail(usuario.getCorreoRevisor(), usuario.getUrlShare());
		return usuarioService.registerShare(usuario);
	}
	
	@GetMapping("/getByIdProyecto")
	List<Usuario> getByIdProyecto(@RequestParam(value="idProyecto") String idProyecto) {
		return usuarioService.getByIdProyecto(idProyecto);
	}
	
	

}
