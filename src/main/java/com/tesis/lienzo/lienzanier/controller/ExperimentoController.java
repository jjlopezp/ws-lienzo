package com.tesis.lienzo.lienzanier.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tesis.lienzo.lienzanier.bean.Experimento;
import com.tesis.lienzo.lienzanier.bean.ProyectoArea;
import com.tesis.lienzo.lienzanier.bean.PruebaHipotesis;
import com.tesis.lienzo.lienzanier.dto.ExperimentoDto;
import com.tesis.lienzo.lienzanier.service.ExperimentoService;
import com.tesis.lienzo.lienzanier.service.LienzoService;
import com.tesis.lienzo.lienzanier.util.Constantes;

@RestController
@CrossOrigin
@RequestMapping("/experimento")
public class ExperimentoController {
	
	@Autowired
	ExperimentoService experimentoService;
	
	@Autowired
	LienzoService lienzoService;
	
	@GetMapping("getPruebas")
	public List<PruebaHipotesis> getTipoPruebas(){
		return experimentoService.getTipoPruebas();
	}
	
	@PostMapping("/save")
	ResponseEntity<Integer> saveExperimento(@RequestBody ExperimentoDto experimentoDto) {
		Integer newIdExperimento = experimentoService.save(experimentoDto);
		
		return new ResponseEntity<Integer>(newIdExperimento, HttpStatus.OK);
	}
	
	@PostMapping("/guardarCambiosExperimento")
	public void guardarCambiosExperimento(@RequestBody ExperimentoDto experimentoDto) {
		experimentoService.guardarCambiosExperimento(experimentoDto);
		
		
	}
	
	@GetMapping("getByProjecto")
	public List<Experimento> getByProjecto(@RequestParam(value="id") String idProyecto){
		return experimentoService.getByProjecto(idProyecto);
	}
	
	@GetMapping("updateEstado")
	public ResponseEntity<String> updateEstado(@RequestParam(value="id") String idExperimento,
												@RequestParam(value="estado") String estado){
		
		experimentoService.updateEstado(idExperimento, estado);
		
		return new ResponseEntity<String>("Ok", HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("getDetalleExperimento")
	public Experimento getDetalleExperimento(@RequestParam(value="id") String idExperimento) {
		return experimentoService.getDetalleExperimento(idExperimento);
	}

	@GetMapping("/getByProyectoAndEstado")
	List<ProyectoArea> getByProyectoAndEstado(@RequestParam(value="id") String idProyecto, @RequestParam(value="estado") String estado){
		
		return experimentoService.getByProyectoAndEstado(idProyecto, estado);
	}
	
	@PostMapping("/saveMediciones")
	@Transactional
	ResponseEntity<String> saveMediciones(@RequestBody Experimento experimento) {
		experimentoService.updateEstado(String.valueOf(experimento.getId()), String.valueOf(Constantes.ESTADO_EXPERIMENTO_EN_CURSO_VIEJO));
		lienzoService.updateEstado(experimento.getIdHipotesis(), Constantes.ESTADO_HIPOTESIS_EN_CURSO_VIEJO);
		experimentoService.saveMediciones(experimento);
		
		return new ResponseEntity<String>("Ok", HttpStatus.OK);
	}
	
	@PostMapping("/updateMediciones")
	@Transactional
	ResponseEntity<String> updateMediciones(@RequestBody Experimento experimento) {

		experimentoService.saveMediciones(experimento);
		
		return new ResponseEntity<String>("Ok", HttpStatus.OK);
	}
	
	@GetMapping("/finalizarExperimento")
	void finalizarExperimento(@RequestParam(value="idHipotesis") String idHipotesis,
								@RequestParam(value="idProyecto") String idProyecto,
								@RequestParam(value="idExperimento") String idExperimento){
		
		experimentoService.finalizarExperimento(idHipotesis, idExperimento);
	}
	
}
