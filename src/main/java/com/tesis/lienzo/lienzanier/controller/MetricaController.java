package com.tesis.lienzo.lienzanier.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tesis.lienzo.lienzanier.bean.Metrica;
import com.tesis.lienzo.lienzanier.bean.Proyecto;
import com.tesis.lienzo.lienzanier.service.MetricaService;

@RestController
@CrossOrigin
@RequestMapping("/metrica")
public class MetricaController {

	@Autowired
	MetricaService metricaService;
	
	@GetMapping("/getMetricaByProyecto")
	List<Metrica> getByProyecto(@RequestParam(value="id") String idProyecto){
		
		return metricaService.getMetricasByIdProyecto(idProyecto);
	}
	
	@PostMapping("/save")
	int saveUsuario(@RequestBody Metrica proyecto) {
		return metricaService.save(proyecto);
	}
	
	@PostMapping("/update")
	String update(@RequestBody Metrica proyecto) {
		return metricaService.update(proyecto);
	}
	
	@GetMapping("/delete")
	void deleteById(@RequestParam(value="id") String id) {
		metricaService.delete(id);
	}
	
}
