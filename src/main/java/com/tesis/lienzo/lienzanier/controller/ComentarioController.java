package com.tesis.lienzo.lienzanier.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tesis.lienzo.lienzanier.bean.Comentario;
import com.tesis.lienzo.lienzanier.service.ComentarioService;

@RestController
@CrossOrigin
@RequestMapping("/comentario")
public class ComentarioController {

	
	@Autowired 
	ComentarioService comentarioService;
	
	@GetMapping("/getComentariosByProyecto")
	public List<Comentario> getComentariosByProyecto(@RequestParam(value="idProyecto") String idProyecto){
		return comentarioService.getComentariosByProyecto(idProyecto);
	}
	
	@GetMapping("/updateEstadoComentario")
	public void updateEstadoComentario(@RequestParam(value="idComentario") String idComentario, @RequestParam(value="estado") String estado){
		comentarioService.updateEstadoComentario(idComentario, estado);
	}
	
	@PostMapping("/saveComentarioProyecto")
	public void saveComentarioProyecto(@RequestBody Comentario comentario) {
		comentarioService.saveComentarioProyecto(comentario);
	}
	
	@GetMapping("/getComentariosByExperimento")
	public List<Comentario> getComentariosByExperimento(@RequestParam(value="idExperimento") String idExperimento){
		return comentarioService.getComentariosByExperimento(idExperimento);
	}
	
	@PostMapping("/saveComentarioExperimento")
	public void saveComentarioExperimento(@RequestBody Comentario comentario) {
		comentarioService.saveComentarioExperimento(comentario);
	}
	
	@GetMapping("/updateEstadoComentarioExperimento")
	public void updateEstadoComentarioExperimento(@RequestParam(value="idComentario") String idComentario, @RequestParam(value="estado") String estado){
		comentarioService.updateEstadoComentarioExperimento(idComentario, estado);
	}
	
	@GetMapping("/getComentariosDetalleByProyecto")
	public List<Comentario> getComentariosDetalleByProyecto(@RequestParam(value="idProyecto") String idProyecto){
		return comentarioService.getComentariosDetalleByProyecto(idProyecto);
	}
	
	
	
}
