package com.tesis.lienzo.lienzanier.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tesis.lienzo.lienzanier.bean.Arbol;
import com.tesis.lienzo.lienzanier.bean.Hipotesis;
import com.tesis.lienzo.lienzanier.bean.HipotesisArbol;
import com.tesis.lienzo.lienzanier.bean.NodoArbol;
import com.tesis.lienzo.lienzanier.bean.ProyectoArea;
import com.tesis.lienzo.lienzanier.bean.Relacion;
import com.tesis.lienzo.lienzanier.dto.HipotesisDto;
import com.tesis.lienzo.lienzanier.service.LienzoService;
import com.tesis.lienzo.lienzanier.util.Constantes;

@RestController
@CrossOrigin
@RequestMapping("/lienzo")
public class LienzoController {

	@Autowired
	LienzoService lienzoService;
	
	
	@GetMapping("/getByProyecto")
	List<ProyectoArea> getByProyecto(@RequestParam(value="id") String idProyecto){
		
		return lienzoService.getByProyecto(idProyecto);
	}
	
	@PostMapping("/save")
	String saveUsuario(@RequestBody HipotesisDto hipotesisDto) {
		return lienzoService.save(hipotesisDto);
	}
	
	@GetMapping("/getHipotesisById")
	HipotesisDto getHipotesisById(@RequestParam(value="id") String idHipotesis){
		
		return lienzoService.getHipotesisById(idHipotesis);
	}
	
	@PostMapping("/updateHipotesis")
	String updateHipotesis(@RequestBody HipotesisDto hipotesisDto) {
		/*Update y redirige a Crear Experimento -> pone a hipostesis en estado 2*/
		lienzoService.updateHipotesis(hipotesisDto);
		lienzoService.updateEstado(hipotesisDto.getId(), Constantes.ESTADO_HIPOTESIS_CREAR_EXPERIMENTO);
//		if(hipotesisDto.getIdHipotesisPadre() != 0) {
		System.out.println("ID PROYECTO DE HIPOTESIS EDITAR " + hipotesisDto.getIdProyecto());
			lienzoService.insertarRelacion(hipotesisDto.getIdHipotesisPadre(), hipotesisDto.getId(), hipotesisDto.getIdProyecto()); //PADRE | HIJO
//		}
		return "ok";
	}
	
	@PostMapping("/guardarCambiosHipotesis")
	String guardarCambiosHipotesis(@RequestBody HipotesisDto hipotesisDto) {
		
		lienzoService.updateHipotesis(hipotesisDto);
		return "ok";
	}
	
	@GetMapping("/arbolHipotesis")
	List<NodoArbol> arbolHipotesisByHipotesis(@RequestParam(value="idProyecto") String idProyecto){
		List<Relacion> relaciones = lienzoService.getRelaciones(idProyecto);

		Arbol t = new Arbol(new NodoArbol(-1)); 
		

		Map<Integer, Arbol> response =  t.construirArbol(relaciones);
		
		
		List<NodoArbol> nodos = new ArrayList<>();
		
		for (Map.Entry<Integer, Arbol> entry : response.entrySet()) {
			
			Arbol cabeza = entry.getValue();
			NodoArbol nodoCabeza = cabeza.getRoot();
			nodos.add(nodoCabeza);
			
			nodoCabeza.printTreeIdented(lienzoService);

		}
		return nodos;
	}
	
	@GetMapping("/getHipotesisByProyecto")
	List<Hipotesis> getHipotesisByProyecto(@RequestParam(value="id") String idProyecto){
		
		return lienzoService.getHipotesisByProyecto(idProyecto);
	}
	
	
	
	
}
