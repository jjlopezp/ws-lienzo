package com.tesis.lienzo.lienzanier.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tesis.lienzo.lienzanier.bean.Proyecto;
import com.tesis.lienzo.lienzanier.service.ProyectoService;

@RestController
@CrossOrigin
@RequestMapping("/proyecto")
public class ProyectoController {

	@Autowired
	ProyectoService proyectoService;
	
	
	@GetMapping("/getAll")
	List<Proyecto> getAllProyectos() {
		return proyectoService.getAll();
	}
	
	@GetMapping("/getByUser")
	List<Proyecto> getByUser(@RequestParam(value="id") String id) {
		return proyectoService.getByUser(id);
	}
	
	@PostMapping("/save")
	String saveUsuario(@RequestBody Proyecto proyecto) {
		return proyectoService.save(proyecto);
	}
	
	@GetMapping("/delete")
	void deleteById(@RequestParam(value="id") String id) {
		proyectoService.deleteById(id);
	}
	
	@PostMapping("/update")
	void update(@RequestBody Proyecto proyecto) {
		proyectoService.update(proyecto);
	}
	
	@GetMapping("/getById")
	Proyecto getById(@RequestParam(value="id") String id) {
		return proyectoService.getById(id);
	}
	
}
