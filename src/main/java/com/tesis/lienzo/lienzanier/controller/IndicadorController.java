package com.tesis.lienzo.lienzanier.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tesis.lienzo.lienzanier.bean.Experimento;
import com.tesis.lienzo.lienzanier.bean.Indicador;
import com.tesis.lienzo.lienzanier.service.IndicadorService;
import com.tesis.lienzo.lienzanier.util.Constantes;

@RestController
@CrossOrigin
@RequestMapping("/indicador")
public class IndicadorController {

	@Autowired
	IndicadorService indicadorService;
	
	@GetMapping("/listar")
	public List<Indicador> listar(@RequestParam(value="idProyecto") String idProyecto){
		return indicadorService.listar(idProyecto);
	}
	
	@PostMapping("/save")
	public void save(@RequestBody Indicador indicador) {
		indicadorService.save(indicador);
	}
	
	@GetMapping("/getIndicador")
	public Indicador getIndicador(@RequestParam(value="idIndicador") String idIndicador) {
		return indicadorService.getIndicador(idIndicador);
	}
	
	@PostMapping("/saveMediciones")
	@Transactional
	ResponseEntity<String> saveMediciones(@RequestBody Indicador indicador) {
		indicadorService.saveMediciones(indicador);
		
		return new ResponseEntity<String>("Ok", HttpStatus.OK);
	}
}
