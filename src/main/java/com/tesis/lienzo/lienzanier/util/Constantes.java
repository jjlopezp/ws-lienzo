package com.tesis.lienzo.lienzanier.util;

public class Constantes {
	
	public static final int ID_SOCIOS_CLAVE = 1;
	public static final int ID_ACTIVIDADES_CLAVE = 2;
	public static final int ID_RECURSOS_CLAVE = 3;
	public static final int ID_PROPUESTA_VALOR = 4;
	public static final int ID_RELACION_CLIENTES = 5;
	public static final int ID_CANALES = 6;
	public static final int ID_SEGMENTOS_CLIENTES = 7;
	public static final int ID_ESTRUCTURA_COSTOS = 8;
	public static final int ID_FUENTES_INGRESO = 9;
	
	public static final int ESTADO_EXPERIMENTO_ELIMINADO = 0;
	public static final int ESTADO_EXPERIMENTO_EN_CURSO_NUEVO = 3;
	public static final int ESTADO_EXPERIMENTO_EN_CURSO_VIEJO = 4;
	public static final int ESTADO_EXPERIMENTO_FINALIZADO = 5;
	
	public static final int ESTADO_HIPOTESIS_ELIMINADO = 0;
	public static final int ESTADO_HIPOTESIS_ACTIVO = 1;
	public static final int ESTADO_HIPOTESIS_CREAR_EXPERIMENTO = 2;
	public static final int ESTADO_HIPOTESIS_EN_CURSO_NUEVO = 3;
	public static final int ESTADO_HIPOTESIS_EN_CURSO_VIEJO = 4;
	public static final int ESTADO_HIPOTESIS_FINALIZADO = 5;
}
