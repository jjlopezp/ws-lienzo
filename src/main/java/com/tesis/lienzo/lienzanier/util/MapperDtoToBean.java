package com.tesis.lienzo.lienzanier.util;

import org.springframework.stereotype.Component;

import com.tesis.lienzo.lienzanier.bean.Hipotesis;
import com.tesis.lienzo.lienzanier.dto.HipotesisDto;

@Component
public class MapperDtoToBean {

	public HipotesisDto convertHipotesisToHipotesisDto(Hipotesis hipotesis) {
		HipotesisDto hipotesisDto = new HipotesisDto();
		
		hipotesisDto.setId(hipotesis.getId());
		hipotesisDto.setSuposicion(hipotesis.getSuposicion());
		hipotesisDto.setDescripcion(hipotesis.getDescripcion());
		hipotesisDto.setDuracion(hipotesis.getDuracion());
		hipotesisDto.setEstado(hipotesis.getEstado());
		hipotesisDto.setPublico(hipotesis.getPublico());
		hipotesisDto.setFechaRegistro(hipotesis.getFechaRegistro());
		
		
		return hipotesisDto;
	}

}
