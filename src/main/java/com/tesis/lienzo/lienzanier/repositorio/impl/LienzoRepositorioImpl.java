package com.tesis.lienzo.lienzanier.repositorio.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tesis.lienzo.lienzanier.bean.Hipotesis;
import com.tesis.lienzo.lienzanier.bean.NodoArbol;
import com.tesis.lienzo.lienzanier.bean.ProyectoArea;
import com.tesis.lienzo.lienzanier.bean.Relacion;
import com.tesis.lienzo.lienzanier.bean.Usuario;
import com.tesis.lienzo.lienzanier.dto.HipotesisDto;
import com.tesis.lienzo.lienzanier.repositorio.LienzoRepositorio;
import com.tesis.lienzo.lienzanier.util.Constantes;

@Repository
public class LienzoRepositorioImpl implements LienzoRepositorio{

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Override
	@Transactional
	public String save(HipotesisDto hipotesisDto) {
		
		KeyHolder holder = new GeneratedKeyHolder();
		

		String sql = "insert into hipotesis (suposicion, publico, duracion, estado, fecha_reg) "
				+ "values (?, ?, ?, 1, current_date)";
		int r = jdbcTemplate.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, hipotesisDto.getSuposicion());
				ps.setString(2, hipotesisDto.getPublico());
				ps.setInt(3, hipotesisDto.getDuracion());
				return ps;
			}
		}, holder);

		int newHipotesisId = (int) holder.getKeys().get("id");
		
		
		/*Insertar batch usuarios_proyecto*/
		List<ProyectoArea> idProyectoAreas = hipotesisDto.getProyectoAreas();
		int[] updateCounts = jdbcTemplate.batchUpdate(
                "insert into proyecto_area_hipotesis (id_proyecto_area, id_hipotesis) "
                + "values (?, ?)",
                new BatchPreparedStatementSetter() {
                    public void setValues(PreparedStatement ps, int i) throws SQLException {
                        ps.setInt(1, idProyectoAreas.get(i).getId());
                        ps.setInt(2, newHipotesisId);
                    }

                    public int getBatchSize() {
                        return idProyectoAreas.size();
                    }
                } );
		
		if(r == 1)
			return "OK";
		return "NO";
	}

	@Override
	public List<ProyectoArea> getAreaByProyecto(String idProyecto) {
	
		String sql = "select * from proyecto_area"
				+ " where id_proyecto="+idProyecto;
		
		List<ProyectoArea> areasXproyecto = new ArrayList<>();
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		

		for (Map row : rows) {
			ProyectoArea obj = new ProyectoArea();
			obj.setId((Integer)row.get("ID"));
			obj.setIdProyecto((Integer)row.get("ID_PROYECTO"));
			obj.setIdArea((Integer)row.get("ID_AREA"));
		
			areasXproyecto.add(obj);
		}
		
		return areasXproyecto;
	
	}

	@Override
	public List<Hipotesis> getHipotesisByProyectoArea(String idProyectoArea) {
		
		
		String sql = "select hi.id, hi.suposicion, hi.publico, hi.duracion, hi.estado from hipotesis hi"
				+ " inner join proyecto_area_hipotesis pah on hi.id = pah.id_hipotesis"
				+ " where pah.id_proyecto_area="+idProyectoArea+" and (hi.estado="+Constantes.ESTADO_HIPOTESIS_ACTIVO+" or hi.estado="+Constantes.ESTADO_HIPOTESIS_CREAR_EXPERIMENTO+")";

		List<Hipotesis> hipotesis = new ArrayList<>();
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		

		for (Map row : rows) {
			Hipotesis obj = new Hipotesis();
			obj.setId((Integer)row.get("ID"));
			obj.setSuposicion((String)row.get("suposicion"));
			obj.setPublico((String)row.get("publico"));
			obj.setDuracion((Integer)row.get("duracion"));
			obj.setEstado((Integer)row.get("estado"));
		
			hipotesis.add(obj);
		}
		
		return hipotesis;
	}

	@Override
	public Hipotesis getHipotesisById(String id) {
		String sql = "select * from hipotesis"
				+ " where id="+id;

		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		

		for (Map row : rows) {
			Hipotesis obj = new Hipotesis();
			obj.setId((Integer)row.get("ID"));
			obj.setSuposicion((String)row.get("suposicion"));
			obj.setPublico((String)row.get("publico"));
			obj.setDuracion((Integer)row.get("duracion"));
			obj.setEstado((Integer)row.get("estado"));
			obj.setDescripcion((String)row.get("descripcion"));
			java.sql.Date fecha = (Date)row.get("fecha_reg");
			if(fecha == null) {
				obj.setFechaRegistro("-");
			}else {
				obj.setFechaRegistro(fecha.toString());
			}
//			obj.setFechaRegistro(   ((Date)row.get("fecha_reg")).toString() );
			return obj;
		}
		return new Hipotesis();
	}

	@Override
	public List<ProyectoArea> getProyectoAreaByHipotesis(String idHipotesis) {
		String sql = "select pa.id, pa.id_area from proyecto_area pa"
				+ " inner join proyecto_area_hipotesis pah on pa.id = pah.id_proyecto_area"
				+ " and pah.id_hipotesis ="+idHipotesis;
		
		List<ProyectoArea> proyectoAreas = new ArrayList<>();
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		

		for (Map row : rows) {
			ProyectoArea obj = new ProyectoArea();
			obj.setId((Integer)row.get("ID"));
			obj.setIdArea((Integer)row.get("id_area"));
		
			proyectoAreas.add(obj);
		}
		
		return proyectoAreas;

	}

	@Override
	@Transactional
	public String updateHipotesis(HipotesisDto hipotesisDto) {
		String sql = "update hipotesis"
				+ " set suposicion = ?,"
				+ " 	duracion = ?,"
				+ "		descripcion = ?"
				+ " where id = ?";

		jdbcTemplate.update(sql, hipotesisDto.getSuposicion(), hipotesisDto.getDuracion(), hipotesisDto.getDescripcion(), hipotesisDto.getId());
		
		String sql2 = "delete from proyecto_area_hipotesis"
				+ " where id_hipotesis='"+hipotesisDto.getId()+"'";
		jdbcTemplate.update(sql2);
		
		List<ProyectoArea> idProyectoAreas = hipotesisDto.getProyectoAreas();
		int[] updateCounts = jdbcTemplate.batchUpdate(
                "insert into proyecto_area_hipotesis (id_proyecto_area, id_hipotesis) "
                + "values (?, ?)",
                new BatchPreparedStatementSetter() {
                    public void setValues(PreparedStatement ps, int i) throws SQLException {
                        ps.setInt(1, idProyectoAreas.get(i).getId());
                        ps.setInt(2, hipotesisDto.getId());
                    }

                    public int getBatchSize() {
                        return idProyectoAreas.size();
                    }
                } );
		
		return "OK";
	}
	
	@Override
	public void updateEstado(int idHipotesis, int estado) {
		// TODO Auto-generated method stub
		String sql = "update hipotesis"
				+ " set estado = ?"
				+ " where id = ?";

//		jdbcTemplate.update(sql, Integer.getInteger(estado), Integer.getInteger(idExperimento));
		jdbcTemplate.update(sql, estado, idHipotesis);
	}

	
	@Override
	public List<Hipotesis> getHipotesisByProyecto (String idHProyecto) {
		String sql = "select h.id as idHipotesis, h.suposicion as suposicion, h.estado as estadoHipotesis from proyecto_area pa"
				+ " inner join proyecto_area_hipotesis pah on pah.id_proyecto_area = pa.id"
				+ " inner join hipotesis h on h.id = pah.id_hipotesis"
				+ " where pa.id_proyecto ="+idHProyecto + " and h.estado=1";
		
		List<Hipotesis> hipotesis = new ArrayList<>();
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		

		for (Map row : rows) {
			Hipotesis obj = new Hipotesis();
			obj.setId((Integer)row.get("idHipotesis"));
			obj.setSuposicion((String)row.get("suposicion"));
			obj.setEstado((Integer)row.get("estadohipotesis"));
			
			hipotesis.add(obj);
		}

		
		String sql2 = "select h.id as idhipotesis, h.suposicion as suposicion, h.estado as estadoHipotesis, ex.id as idexperimento from proyecto_area pa"
				+ " inner join proyecto_area_hipotesis pah on pah.id_proyecto_area = pa.id"
				+ " inner join hipotesis h on h.id = pah.id_hipotesis"
				+ " inner join experimento ex on ex.id_hipotesis = h.id"
				+ " where pa.id_proyecto ="+idHProyecto + " and h.estado<>1 and h.estado<>0"
				+ " order by h.estado";

	
		List<Map<String, Object>> rows2 = jdbcTemplate.queryForList(sql2);
		

		for (Map row : rows2) {
			Hipotesis obj = new Hipotesis();
			obj.setId((Integer)row.get("idhipotesis"));
			obj.setSuposicion((String)row.get("suposicion"));
			obj.setEstado((Integer)row.get("estadohipotesis"));
			obj.setIdExperimento((Integer)row.get("idexperimento"));
			hipotesis.add(obj);
		}
		
		return hipotesis;

	}

	@Override
	public void fillNodoHipotesis(NodoArbol nodoArbol) {
		String sql = "select * from hipotesis"
				+ " where id="+nodoArbol.getId();

		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);

		for (Map row : rows) {
			nodoArbol.setSuposicion((String)row.get("suposicion"));			
			nodoArbol.setDescription((String)row.get("suposicion"));
			break;
		}
		
		
	}

	@Override
	public List<Relacion> getRelaciones(String idProyecto) {
		// TODO Auto-generated method stub
		String sql = "select * from hipotesis_rel"
				+ " where id_proyecto="+idProyecto;
		
		List<Relacion> relaciones = new ArrayList<>();
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		

		for (Map row : rows) {
			Relacion obj = new Relacion();
			obj.setIdPadre((Integer)row.get("id_padre"));
			obj.setIdHijo((Integer)row.get("id_hijo"));
			relaciones.add(obj);
		}
		
		String sql2 = "select hi.id from hipotesis hi"
				+ " inner join proyecto_area_hipotesis pah on pah.id_hipotesis = hi.id"
				+ " inner join proyecto_area pa on pa.id = pah.id_proyecto_area"
				+ " where pa.id_proyecto="+idProyecto+" and hi.id NOT IN"
				+ " (select id_padre from hipotesis_rel WHERE id_padre IS NOT NULL)"
				+ " and hi.id NOT IN (select id_hijo from hipotesis_rel where id_hijo IS NOT NULL)";
		
		
		List<Map<String, Object>> rows2 = jdbcTemplate.queryForList(sql2);
		

		for (Map row : rows2) {
			Relacion obj = new Relacion();
			obj.setIdPadre((Integer)row.get("id"));
			obj.setIdHijo(0);
			relaciones.add(obj);
		}
		
	
		
		return relaciones;
	}

	@Override
	public void insertarRelacion(int idHipotesisPadre, int idHijo, int idProyecto) {
		
		/*Se elimina la relación actual del hijo para insertar la nueva relacion*/
		
		String sql2 = "delete from hipotesis_rel"
				+ " where id_hijo='"+idHijo+"'";
		jdbcTemplate.update(sql2);
		
		if(idHipotesisPadre != 0) {
			String sql = "insert into hipotesis_rel(id_padre, id_hijo, id_proyecto)"
					+ " values (?,?, ?)";

			jdbcTemplate.update(sql, idHipotesisPadre, idHijo, idProyecto);
		}
		
		
		
	}
	
	
	
}
