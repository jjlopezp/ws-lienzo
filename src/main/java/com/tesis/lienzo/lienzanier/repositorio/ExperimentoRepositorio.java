package com.tesis.lienzo.lienzanier.repositorio;

import java.util.List;

import com.tesis.lienzo.lienzanier.bean.Experimento;
import com.tesis.lienzo.lienzanier.bean.Hipotesis;
import com.tesis.lienzo.lienzanier.bean.ProyectoArea;
import com.tesis.lienzo.lienzanier.bean.PruebaHipotesis;
import com.tesis.lienzo.lienzanier.dto.ExperimentoDto;

public interface ExperimentoRepositorio {

	Integer save(ExperimentoDto experimentoDto);

	List<PruebaHipotesis> getTipoPruebas();

	List<Experimento> getByProjecto(String idProyecto);

	void updateEstado(int idExperimento, int estado);

	Experimento getDetalleExperimento(String idExperimento);

	List<ProyectoArea> getByProyectoAndEstado(String idProyecto, String estado);

	List<ProyectoArea> getAreaByProyecto(String idProyecto);

	List<Experimento> getExperimentoByProyectoArea(String valueOf, String estado);

	void saveMediciones(Experimento experimento);

	void deleteFechaAndMediciones(Experimento experimento);

	List<Experimento> getExperimentoByProyectoAreaTodos(String valueOf);

	void guardarCambiosExperimento(ExperimentoDto experimentoDto);

}
