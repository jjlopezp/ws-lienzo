package com.tesis.lienzo.lienzanier.repositorio.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tesis.lienzo.lienzanier.bean.Metrica;
import com.tesis.lienzo.lienzanier.repositorio.MetricaRepositorio;

@Repository
public class MetricaRepositorioImpl implements MetricaRepositorio{

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Override
	public List<Metrica> getMetricasByIdProyecto(String idProyecto) {
		String sql = "select me.id, me.nombre, me.nombre_corto, me.descripcion from metrica me"
				+ " where me.id_proyecto="+idProyecto + " and me.estado=1";
		
		List<Metrica> metricas = new ArrayList<>();
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		

		for (Map row : rows) {
			Metrica obj = new Metrica();
			obj.setId((Integer)row.get("ID"));
			obj.setNombre((String)row.get("nombre"));
			obj.setNombreCorto((String)row.get("nombre_corto"));
		
			metricas.add(obj);
		}
		
		return metricas;
	}

	@Override
	@Transactional
	public int save(Metrica metrica) {

		KeyHolder holder = new GeneratedKeyHolder();
		
		String sql = "insert into metrica (nombre, nombre_corto, descripcion, estado, id_proyecto) "
				+ "values (?, ?, ?, ?, ?)";
		int r = jdbcTemplate.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, metrica.getNombre());
				ps.setString(2, metrica.getNombreCorto());
				ps.setString(3, metrica.getDescripcion());
				ps.setInt(4, 1);
				ps.setInt(5, metrica.getIdProyecto());
				return ps;
			}
		}, holder);
//
		int newMetricaId = (int) holder.getKeys().get("id");
//		
//		String sql2 = "insert into metrica_proyecto(id_proyecto, id_metrica)"
//				+ " values (?,?)";
//		
//		jdbcTemplate.update(sql2, metrica.getIdProyecto(), newMetricaId);
		return newMetricaId;
	}

	@Override
	public String update(Metrica metrica) {
		String sql2 = "update metrica "
				+ " set nombre = ?,"
				+ "     nombre_corto = ?,"
				+ "     descripcion = ?"
				+ " where id = ?";
		
		jdbcTemplate.update(sql2, metrica.getNombre(), metrica.getNombreCorto(), metrica.getDescripcion(), metrica.getId());
		
		return "OK";
	}

	@Override
	public void delete(String id) {
		// TODO Auto-generated method stub
		String sql2 = "update metrica "
				+ " set estado = 0"
				+ " where id = ?";
		jdbcTemplate.update(sql2, Integer.valueOf(id));
	}

}
