package com.tesis.lienzo.lienzanier.repositorio;

import com.tesis.lienzo.lienzanier.dto.HipotesisDto;

import java.util.List;

import com.tesis.lienzo.lienzanier.bean.Hipotesis;
import com.tesis.lienzo.lienzanier.bean.NodoArbol;
import com.tesis.lienzo.lienzanier.bean.ProyectoArea;
import com.tesis.lienzo.lienzanier.bean.Relacion;

public interface LienzoRepositorio {

	String save(HipotesisDto hipotesisDto);
	List<ProyectoArea> getAreaByProyecto(String idProyecto);
	List<Hipotesis> getHipotesisByProyectoArea(String idProyectoArea);
	Hipotesis getHipotesisById(String id);
	List<ProyectoArea> getProyectoAreaByHipotesis(String idHipotesis);
	String updateHipotesis(HipotesisDto hipotesisDto);
	void updateEstado(int idHipotesis, int estado);
	List<Hipotesis> getHipotesisByProyecto(String idProyecto);
	void fillNodoHipotesis(NodoArbol nodoArbol);
	List<Relacion> getRelaciones(String idProyecto);
	void insertarRelacion(int idHipotesisPadre, int idHijo, int idProyecto);
}
