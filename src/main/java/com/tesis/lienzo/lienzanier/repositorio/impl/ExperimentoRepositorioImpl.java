package com.tesis.lienzo.lienzanier.repositorio.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tesis.lienzo.lienzanier.bean.Condicion;
import com.tesis.lienzo.lienzanier.bean.Dia;
import com.tesis.lienzo.lienzanier.bean.Experimento;
import com.tesis.lienzo.lienzanier.bean.Fecha;
import com.tesis.lienzo.lienzanier.bean.Hipotesis;
import com.tesis.lienzo.lienzanier.bean.Metrica;
import com.tesis.lienzo.lienzanier.bean.ProyectoArea;
import com.tesis.lienzo.lienzanier.bean.PruebaHipotesis;
import com.tesis.lienzo.lienzanier.bean.PuntoMedicion;
import com.tesis.lienzo.lienzanier.dto.ExperimentoDto;
import com.tesis.lienzo.lienzanier.repositorio.ExperimentoRepositorio;
import com.tesis.lienzo.lienzanier.util.Constantes;

@Repository
public class ExperimentoRepositorioImpl implements ExperimentoRepositorio{

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Override
	@Transactional
	public Integer save(ExperimentoDto experimentoDto) {
		// TODO Auto-generated method stub
		KeyHolder holder = new GeneratedKeyHolder();
		
		String sql = "insert into experimento (id_hipotesis, id_tipo_prueba, descripcion, estado, id_proyecto) "
				+ "values (?, ?, ?, ?, ?)";
		int r = jdbcTemplate.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1, experimentoDto.getIdHipotesis());
				ps.setInt(2, experimentoDto.getIdTipoPrueba());
				ps.setString(3, experimentoDto.getDescripcion());
				ps.setInt(4, Constantes.ESTADO_EXPERIMENTO_EN_CURSO_NUEVO);
				ps.setInt(5, experimentoDto.getIdProyecto());
				return ps;
			}
		}, holder);
		
		int newExperimentoId = (int) holder.getKeys().get("id");
		
		/*Save metrica_experimento*/
		List<Integer> idMetricas = experimentoDto.getMetricas();
		int[] updateCounts = jdbcTemplate.batchUpdate(
                "insert into metrica_experimento (id_metrica, id_experimento, estado) "
                + "values (?, ?, ?)",
                new BatchPreparedStatementSetter() {
                    public void setValues(PreparedStatement ps, int i) throws SQLException {
                        ps.setInt(1, idMetricas.get(i));
                        ps.setInt(2, newExperimentoId);
                        ps.setInt(3, 1);
                    }

                    public int getBatchSize() {
                        return idMetricas.size();
                    }
                } );
		
		/*Save condicion*/
		List<Condicion> condiciones = experimentoDto.getCondiciones();
		int[] updateCounts2 = jdbcTemplate.batchUpdate(
                "insert into condicion (json_tipo, json_valor, txt_condicion, id_experimento, descripcion, estado) "
                + "values (?, ?, ?, ?, ?, ?)",
                new BatchPreparedStatementSetter() {
                    public void setValues(PreparedStatement ps, int i) throws SQLException {
                        ps.setString(1, condiciones.get(i).getJsonTipo());
                        ps.setString(2, condiciones.get(i).getJsonValor());
                        ps.setString(3, condiciones.get(i).getTxtCondicion());
                        ps.setInt(4, newExperimentoId);
                        ps.setString(5, condiciones.get(i).getDescripcion());
                        ps.setInt(6, 1);
                    }

                    public int getBatchSize() {
                        return condiciones.size();
                    }
                } );
		
		return newExperimentoId;
		
	}

	@Override
	public List<PruebaHipotesis> getTipoPruebas() {
		String sql = "select * from prueba_hipotesis"
				+ " where estado=1";
		
		List<PruebaHipotesis> metricas = new ArrayList<>();
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		

		for (Map row : rows) {
			PruebaHipotesis obj = new PruebaHipotesis();
			obj.setId((Integer)row.get("ID"));
			obj.setNombre((String)row.get("nombre"));
			obj.setDescripcion((String)row.get("descripcion"));
			obj.setNombreList((String)row.get("nombre_list"));
			obj.setNombreUpper((String)row.get("nombre_upper"));
			
			metricas.add(obj);
		}
		
		return metricas;
	}

	@Override
	public List<Experimento> getByProjecto(String idProyecto) {
		String sql = "select ex.id, hi.suposicion, ex.estado from experimento ex"
				+ " inner join hipotesis hi on ex.id_hipotesis = hi.id"
				+ " where ex.id_proyecto='"+idProyecto+"' and ex.estado<>'0'";
		
		List<Experimento> metricas = new ArrayList<>();
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		

		for (Map row : rows) {
			Experimento obj = new Experimento();
			obj.setId((Integer)row.get("ID"));
			obj.setHipotesis((String)row.get("suposicion"));
			obj.setEstado((Integer)row.get("estado"));
		
			metricas.add(obj);
		}
		
		return metricas;
	}

	@Override
	public void updateEstado(int idExperimento, int estado) {
		// TODO Auto-generated method stub
		String sql = "update experimento"
				+ " set estado = ?"
				+ " where id = ?";

//		jdbcTemplate.update(sql, Integer.getInteger(estado), Integer.getInteger(idExperimento));
		jdbcTemplate.update(sql, estado, idExperimento);
	}

	@Override
	public Experimento getDetalleExperimento(String idExperimento) {
		
		String sql = "select ex.id as idExperimento, hi.id as idhipotesis, ex.estado as estadoExperimento, hi.suposicion as txtSuposicion, hi.descripcion as descripcion, phi.nombre as pruebaHipotesis, ex.evidencias as evidencias from experimento ex"
				+ " inner join hipotesis hi on hi.id = ex.id_hipotesis"
				+ " inner join prueba_hipotesis phi on phi.id = ex.id_tipo_prueba"
				+ " where ex.id='"+idExperimento+"'";
		
		List<Experimento> experimentos = new ArrayList<>();
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		
		for (Map row : rows) {
			Experimento obj = new Experimento();
			obj.setId((Integer)row.get("idExperimento"));
			obj.setIdHipotesis((Integer)row.get("idhipotesis"));
			obj.setHipotesis((String)row.get("txtsuposicion"));
			obj.setDescripcion((String)row.get("descripcion"));
			obj.setEstado((Integer)row.get("estadoexperimento"));
			obj.setTipoPrueba((String)row.get("pruebahipotesis"));
			obj.setEvidencias((String)row.get("evidencias"));
			int estado = obj.getEstado();
			if(estado==1) {
				obj.setTxtEstado("Sin iniciar");
			}else if(estado==2) {
				obj.setTxtEstado("En curso");
			}else if(estado==3) {
				obj.setTxtEstado("Éxito");
			}else if(estado==4) {
				obj.setTxtEstado("Fracaso");
			}
			experimentos.add(obj);
		}
		
		Experimento experimentoResponse = experimentos.get(0);
		
		
		/*Obtener condiciones*/
		String sql2 = "select * from condicion"
				+ " where id_experimento='"+idExperimento+"' and estado=1";
		
		List<Condicion> condiciones = new ArrayList<>();
		List<Map<String, Object>> rows2 = jdbcTemplate.queryForList(sql2);
		

		for (Map row : rows2) {
			Condicion obj = new Condicion();
			obj.setId((Integer)row.get("ID"));
			obj.setTxtCondicion((String)row.get("txt_condicion"));
			obj.setIdExperimento((Integer)row.get("id_experimento"));
			obj.setDescripcion((String)row.get("descripcion"));
			condiciones.add(obj);
		}
		
		experimentoResponse.setCondiciones(condiciones);
		
		/*Obtenemos Fechas*/
		String sql4 = "select * from fecha"
				+ " where id_experimento='"+idExperimento+"' order by fecha";
		
		List<Fecha> fechas = new ArrayList<>();
		List<Map<String, Object>> rows4 = jdbcTemplate.queryForList(sql4);
		

		for (Map row : rows4) {
			Fecha obj = new Fecha();
			obj.setId((Integer)row.get("ID"));
			obj.setFecha((String) row.get("fecha"));
			obj.setMes((Integer)row.get("mes"));
			
			String sql5 = "select * from dia"
					+ " where id_fecha='"+obj.getId()+"' order by dia";
			List<Dia> dias = new ArrayList<>();
			List<Map<String, Object>> rows5 = jdbcTemplate.queryForList(sql5);
			for (Map row5 : rows5) {
				Dia dia = new Dia();
				dia.setId((Integer)row5.get("id"));
				dia.setId_fecha((Integer)row5.get("id_fecha"));
				dia.setDia((Integer)row5.get("dia"));
				
				
				/*Obtener punto medicion*/
				String sql3 = "select pm.id as idPuntoMedicion, me.id as idMetricaExperimento, pm.valor, met.nombre_corto as nombreCorto, met.id as idMetrica from punto_medicion pm"
						+ " inner join metrica_experimento me on me.id = pm.id_metrica_experimento"
						+ " inner join metrica met on met.id = me.id_metrica"
						+ " where pm.id_dia='"+dia.getId()+"' and me.estado=1";
				
				List<PuntoMedicion> mediciones = new ArrayList<>();
				List<Map<String, Object>> rows3 = jdbcTemplate.queryForList(sql3);
				

				for (Map row3 : rows3) {
					PuntoMedicion obj3 = new PuntoMedicion();
					obj3.setId((Integer)row3.get("idpuntomedicion"));
					obj3.setIdMetricaExperimento((Integer)row3.get("idmetricaexperimento"));
					obj3.setMetrica((String)row3.get("nombrecorto"));
					obj3.setValor((Integer)row3.get("valor"));
					obj3.setIdMetrica((Integer)row3.get("idmetrica"));
				    obj3.setIdentificador("L"+obj3.getIdMetrica());
					mediciones.add(obj3);
				}
				
				dia.setMediciones(mediciones);
				dias.add(dia);
			}
			obj.setDias(dias);
			
			

			fechas.add(obj);
		}
		
		experimentoResponse.setFechas(fechas);

		
		/*Metricas / Indicadores*/
		String sql5 = "select me.id as id, me.nombre as nombre, me.nombre_corto as nombreCorto from metrica me"
				+ " inner join metrica_experimento mex on mex.id_metrica = me.id"
				+ " where mex.id_experimento='"+idExperimento+"' and mex.estado=1";
		
		List<Metrica> metricas = new ArrayList<>();
		List<Map<String, Object>> rows5 = jdbcTemplate.queryForList(sql5);
		

		for (Map row : rows5) {
			Metrica obj = new Metrica();
			obj.setId((Integer)row.get("ID"));
			obj.setNombre((String)row.get("nombre"));
			obj.setNombreCorto((String)row.get("nombreCorto"));
		
			metricas.add(obj);
		}
		
		experimentoResponse.setMetricas(metricas);
		
		
		return experimentoResponse;
	}
	
	private void updateStateHipotesis(int idHipotesis, int estado) {
		String sql = "update hipotesis"
				+ " set estado = ?"
				+ " where id = ?";
		
		jdbcTemplate.update(sql, estado, idHipotesis);
	}

	@Override
	public List<ProyectoArea> getByProyectoAndEstado(String idProyecto, String estado) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ProyectoArea> getAreaByProyecto(String idProyecto) {
		String sql = "select * from proyecto_area"
				+ " where id_proyecto="+idProyecto;
		
		List<ProyectoArea> areasXproyecto = new ArrayList<>();
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		

		for (Map row : rows) {
			ProyectoArea obj = new ProyectoArea();
			obj.setId((Integer)row.get("ID"));
			obj.setIdProyecto((Integer)row.get("ID_PROYECTO"));
			obj.setIdArea((Integer)row.get("ID_AREA"));
		
			areasXproyecto.add(obj);
		}
		
		return areasXproyecto;
	}

	@Override
	public List<Experimento> getExperimentoByProyectoArea(String idProyectoArea, String estado) {
		String sql = "select ex.id, hi.suposicion, ex.estado from hipotesis hi"
				+ " inner join proyecto_area_hipotesis pah on hi.id = pah.id_hipotesis"
				+ " inner join experimento ex on ex.id_hipotesis = hi.id"
				+ " where pah.id_proyecto_area="+idProyectoArea+" and ex.estado='"+estado+"'";
		
		List<Experimento> hipotesis = new ArrayList<>();
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		

		for (Map row : rows) {
			Experimento obj = new Experimento();
			obj.setId((Integer)row.get("ID"));
			obj.setHipotesis((String)row.get("suposicion"));
			obj.setEstado((Integer)row.get("estado"));
			hipotesis.add(obj);
		}
		
		return hipotesis;
	}

	@Override
	@Transactional
	public void saveMediciones(Experimento experimento) {
		
		//sqlUpdate de evidencias
		String sqlUpdate = "update experimento set evidencias='"+experimento.getEvidencias()+"'"
				+ " where id='"+experimento.getId()+"'";
		jdbcTemplate.update(sqlUpdate);
		
		
		
		int idExperimento = experimento.getId();
		for(Fecha fe : experimento.getFechas()) {
			KeyHolder holder = new GeneratedKeyHolder();
			
			String sql = "insert into fecha (id_experimento, mes) "
					+ "values (?, ?)";
			int r = jdbcTemplate.update(new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
					PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
					ps.setInt(1, experimento.getId());
					ps.setInt(2, fe.getMes());
					return ps;
				}
			}, holder);
			
			int newFechaId = (int) holder.getKeys().get("id");
			
			List<Dia> dias = fe.getDias();
			for(Dia dia: dias) {
				KeyHolder holder3 = new GeneratedKeyHolder();
				String sql3 = "insert into dia (id_fecha, dia, semana) "
						+ "values (?, ?, ?)";
				int r3 = jdbcTemplate.update(new PreparedStatementCreator() {
					@Override
					public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
						PreparedStatement ps = connection.prepareStatement(sql3, Statement.RETURN_GENERATED_KEYS);
						ps.setInt(1, newFechaId);
						ps.setInt(2, dia.getDia());
						ps.setInt(3, dia.getSemana());
						return ps;
					}
				}, holder3);
				
				int newDiaId = (int) holder3.getKeys().get("id");
				
				/*Guardamos punto_medicion*/
				List<PuntoMedicion> mediciones = dia.getMediciones();
				for(PuntoMedicion pm : mediciones) {
					int idMetricaExperimento = getIdMetricaExperimento(pm.getIdMetrica(), idExperimento);
					pm.setIdMetricaExperimento(idMetricaExperimento);
				}
				
				int[] updateCounts = jdbcTemplate.batchUpdate(
		                "insert into punto_medicion (id_dia, id_metrica_experimento, valor) "
		                + "values (?, ?, ?)",
		                new BatchPreparedStatementSetter() {
		                    public void setValues(PreparedStatement ps, int i) throws SQLException {
		                        ps.setInt(1, newDiaId);
		                        ps.setInt(2, mediciones.get(i).getIdMetricaExperimento());
		                        ps.setInt(3, mediciones.get(i).getValor());
		                    }

		                    public int getBatchSize() {
		                        return mediciones.size();
		                    }
		                } );
				
				
				
			}
			
			
			
			
		}
		
		
	}

	private int getIdMetricaExperimento(int idMetrica, int idExperimento) {
		String sql = "select * from metrica_experimento"
				+ " where id_metrica="+idMetrica+ " and id_experimento="+idExperimento + " and estado=1";
		
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		

		for (Map row : rows) {
			return (Integer)row.get("ID");
			
		}
		
		return 0;
	}

	@Override
	@Transactional
	public void deleteFechaAndMediciones(Experimento experimento) {
		// TODO Auto-generated method stub
		String sql = "delete from punto_medicion"
				+ " using fecha, experimento, dia"
				+ " where punto_medicion.id_dia = dia.id"
				+ " and fecha.id = dia.id_fecha"
				+ "	and experimento.id = fecha.id_experimento"
				+ "	and experimento.id='"+experimento.getId()+"'";
		jdbcTemplate.update(sql);
		
		String sq3 = "delete from dia"
				+ " using fecha, experimento"
				+ " where fecha.id = dia.id_fecha"
				+ "	and experimento.id = fecha.id_experimento"
				+ "	and experimento.id='"+experimento.getId()+"'";
		jdbcTemplate.update(sq3);
		
		String sql2 = "delete from fecha"
				+ " where id_experimento='"+experimento.getId()+"'";
		jdbcTemplate.update(sql2);
		
		
	}

	@Override
	public List<Experimento> getExperimentoByProyectoAreaTodos(String valueOf) {
		String sql = "select ex.id, hi.suposicion, hi.id as idHipotesis from hipotesis hi,"
				+ " inner join proyecto_area_hipotesis pah on hi.id = pah.id_hipotesis"
				+ " inner join experimento ex on ex.id_hipotesis = hi.id"
				+ " where pah.id_proyecto_area="+valueOf;
		
		List<Experimento> hipotesis = new ArrayList<>();
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		

		for (Map row : rows) {
			Experimento obj = new Experimento();
			obj.setId((Integer)row.get("ID"));
			obj.setHipotesis((String)row.get("suposicion"));
			obj.setIdHipotesis((Integer)row.get("idHipotesis"));
			hipotesis.add(obj);
		}
		
		return hipotesis;
	}

	@Override
	public void guardarCambiosExperimento(ExperimentoDto experimentoDto) {
		// TODO Auto-generated method stub
		int newExperimentoId = experimentoDto.getId();
		
		
		
//		String sqlDias = "select DISTINCT pm.id_dia from punto_medicion pm"
//				+ " inner join metrica_experimento me on me.id = pm.id_metrica_experimento"
//				+ " where me.id_experimento='"+experimentoDto.getId()+"'";
//		
//		List<Integer> dias = new ArrayList<>();
//		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sqlDias);
//		
//
//		for (Map row : rows) {
//
//			dias.add((Integer)row.get("id_dia"));
//		}
		
		
		if(experimentoDto.getEstado() != Constantes.ESTADO_EXPERIMENTO_EN_CURSO_VIEJO) {
			//Actualizar estado = 0  de metrica_experimento
			String sql4 = "update metrica_experimento"
					+ " set estado = ?"
					+ " where id_experimento = ?";
			jdbcTemplate.update(sql4, 0, experimentoDto.getId());
			
			/*Save metrica_experimento*/
			List<Integer> idMetricas = experimentoDto.getMetricas();
			int[] updateCounts = jdbcTemplate.batchUpdate(
	                "insert into metrica_experimento (id_metrica, id_experimento, estado) "
	                + "values (?, ?, 1)",
	                new BatchPreparedStatementSetter() {
	                    public void setValues(PreparedStatement ps, int i) throws SQLException {
	                        ps.setInt(1, idMetricas.get(i));
	                        ps.setInt(2, newExperimentoId);
	                    }

	                    public int getBatchSize() {
	                        return idMetricas.size();
	                    }
	                } );
		}
		
		
		
		//Actualizar estado = 0 de condición
		String sql5 = "update condicion"
				+ " set estado = ?"
				+ " where id_experimento = ?";
		jdbcTemplate.update(sql5, 0, experimentoDto.getId());
		

		/*Save condicion*/
		List<Condicion> condiciones = experimentoDto.getCondiciones();
		int[] updateCounts2 = jdbcTemplate.batchUpdate(
                "insert into condicion (json_tipo, json_valor, txt_condicion, id_experimento, descripcion, estado) "
                + "values (?, ?, ?, ?, ?, 1)",
                new BatchPreparedStatementSetter() {
                    public void setValues(PreparedStatement ps, int i) throws SQLException {
                        ps.setString(1, condiciones.get(i).getJsonTipo());
                        ps.setString(2, condiciones.get(i).getJsonValor());
                        ps.setString(3, condiciones.get(i).getTxtCondicion());
                        ps.setInt(4, newExperimentoId);
                        ps.setString(5, condiciones.get(i).getDescripcion());
                    }

                    public int getBatchSize() {
                        return condiciones.size();
                    }
                } );
		
		
	}

}
