package com.tesis.lienzo.lienzanier.repositorio.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.tesis.lienzo.lienzanier.bean.Comentario;
import com.tesis.lienzo.lienzanier.repositorio.ComentarioRepository;

@Repository
public class ComentarioRepositoryImpl implements ComentarioRepository{

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public List<Comentario> getComentariosByProyecto(String idProyecto){
		
		String sql2 = "select * from comentario_pro"
				+ " where id_proyecto='"+idProyecto+"' and estado='1'";
		
		List<Comentario> comentarios = new ArrayList<>();
		List<Map<String, Object>> rows2 = jdbcTemplate.queryForList(sql2);
		

		for (Map row : rows2) {
			Comentario obj = new Comentario();
			obj.setId((Integer)row.get("ID"));
			obj.setIdProyecto((Integer)row.get("id_proyecto"));
			obj.setComentario((String)row.get("comentario"));
			comentarios.add(obj);
		}
	
		return comentarios;
	}

	@Override
	public void updateEstadoComentario(String idComentario, String estado) {
		// TODO Auto-generated method stub
		String sql = "update comentario_pro"
				+ " set estado = ?"
				+ " where id = ?";
		
		jdbcTemplate.update(sql, Integer.valueOf(estado), Integer.valueOf(idComentario));
	}

	@Override
	public void saveComentarioProyecto(Comentario comentario) {
		// TODO Auto-generated method stub
		String sql = "insert into comentario_pro (id_proyecto, comentario, estado) "
				+ "values (?, ?, ?)";
		int r = jdbcTemplate.update(sql, comentario.getIdProyecto(), comentario.getComentario(), 1);   

		
	}

	@Override
	public List<Comentario> getComentariosByExperimento(String idExperimento) {
		String sql2 = "select * from comentario"
				+ " where id_experimento='"+idExperimento+"' and estado='1'";
		
		List<Comentario> comentarios = new ArrayList<>();
		List<Map<String, Object>> rows2 = jdbcTemplate.queryForList(sql2);
		

		for (Map row : rows2) {
			Comentario obj = new Comentario();
			obj.setId((Integer)row.get("ID"));
			obj.setIdExperimento((Integer)row.get("id_experimento"));
			obj.setComentario((String)row.get("comentario"));
			comentarios.add(obj);
		}
	
		return comentarios;
	}
	
	@Override
	public void saveComentarioExperimento(Comentario comentario) {
		// TODO Auto-generated method stub
		String sql = "insert into comentario (id_experimento, comentario, estado) "
				+ "values (?, ?, ?)";
		int r = jdbcTemplate.update(sql, comentario.getIdExperimento(), comentario.getComentario(), 1);   

		
	}

	@Override
	public void updateEstadoComentarioExperimento(String idComentario, String estado) {
		// TODO Auto-generated method stub
				String sql = "update comentario"
						+ " set estado = ?"
						+ " where id = ?";
				
				jdbcTemplate.update(sql, Integer.valueOf(estado), Integer.valueOf(idComentario));
		
	}

	@Override
	public List<Comentario> getComentariosDetalleByProyecto(String idProyecto) {
		// TODO Auto-generated method stub
		String sql2 = "select DISTINCT on (1) co.id_experimento as idExperimento, co.id as idComentario, co.comentario as comentario, hi.suposicion as hipotesis, ex.estado as estadoExperimento from comentario co"
				+ " inner join experimento ex on ex.id = co.id_experimento"
				+ " inner join hipotesis hi on ex.id_hipotesis = hi.id"
				+ " where ex.id_proyecto='"+idProyecto+"' and co.estado='1'";
		
		
		
		List<Comentario> comentarios = new ArrayList<>();
		List<Map<String, Object>> rows2 = jdbcTemplate.queryForList(sql2);
		

		for (Map row : rows2) {
			Comentario obj = new Comentario();
			obj.setId((Integer)row.get("idComentario"));
			obj.setIdExperimento((Integer)row.get("idExperimento"));
			obj.setComentario((String)row.get("comentario"));
			obj.setSuposicion((String)row.get("hipotesis"));
			obj.setEstadoExperimento((Integer)row.get("estadoExperimento"));
			comentarios.add(obj);
		}
	
		return comentarios;
	
	}
	
}
