package com.tesis.lienzo.lienzanier.repositorio;

import java.util.List;

import com.tesis.lienzo.lienzanier.bean.Metrica;

public interface MetricaRepositorio {

	List<Metrica> getMetricasByIdProyecto(String idProyecto);

	int save(Metrica proyecto);

	String update(Metrica proyecto);

	void delete(String id);

}
