package com.tesis.lienzo.lienzanier.repositorio.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tesis.lienzo.lienzanier.bean.Proyecto;
import com.tesis.lienzo.lienzanier.bean.Usuario;
import com.tesis.lienzo.lienzanier.bean.UsuarioProyecto;
import com.tesis.lienzo.lienzanier.repositorio.UsuarioRepositorio;

@Repository
public class UsuarioRepositorioImpl implements UsuarioRepositorio{

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<Usuario> findAll() {
		// TODO Auto-generated method stub
		String sql = "SELECT * FROM USUARIO";
		
		List<Usuario> usuarios = new ArrayList<>();
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		

		for (Map row : rows) {
			Usuario obj = new Usuario.UsuarioBuilder((Integer) row.get("ID"), (String) row.get("email"))
					.usuario((String) row.get("usuario"))
					.nombre((String) row.get("nombre"))
					.apellidoPaterno((String) row.get("apellido_paterno"))
					.apellidoMaterno((String) row.get("apellido_materno"))
					.contrasena((String) row.get("contrasena"))
					.build();
	
			usuarios.add(obj);
		}
		
		
		return usuarios;
	}

	@Override
	public String save(Usuario usuario) {
		// TODO Auto-generated method stub
		String sql = "insert into usuario (usuario, nombre, apellido_paterno, apellido_materno, email, rol, contrasena) "
				+ "values (?, ?, ?, ?, ?, ?, ?)";
		int r = jdbcTemplate.update(sql, usuario.getUsuario(), usuario.getNombre(), usuario.getApellidoPaterno(), usuario.getApellidoMaterno(), usuario.getEmail(), usuario.getRol(), usuario.getContrasena());   

		if(r == 1)
			return "OK";
		return "NO";
	}

	@Override
	public Usuario login(Usuario usuario) {
		
		
		String sql = "SELECT * FROM USUARIO"
				+ " where usuario = ? and contrasena = ?";
		
		try {
			Usuario user = jdbcTemplate.queryForObject(sql, new Object[]{usuario.getUsuario(), usuario.getContrasena()}, (rs, rowNum) ->
					        new Usuario.UsuarioBuilder(
					                rs.getInt("id"),
					                rs.getString("email")
					                
					        ).nombre(rs.getString("nombre"))
					        .apellidoPaterno(rs.getString("apellido_paterno"))
					        .apellidoMaterno(rs.getString("apellido_materno"))
					        .rol(rs.getInt("rol"))
					        .build()
	        );
			return user;
		}catch (EmptyResultDataAccessException ex) {
			return new Usuario();
		}

		
		
	}

	@Override
	public List<Usuario> findByEmail(String email, Integer idUser) {
		
		String sql = "SELECT * FROM USUARIO"
				+ " where email like '%"+email+"%' and id!="+idUser;
		
		List<Usuario> usuarios = new ArrayList<>();
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		

		for (Map row : rows) {
			Usuario obj = new Usuario.UsuarioBuilder(
								(Integer)row.get("ID"),
								(String)row.get("email")			                
					        ).nombre((String)row.get("nombre"))
					        .apellidoPaterno((String)row.get("apellido_paterno"))
					        .build();
			

			usuarios.add(obj);
		}
		
		return usuarios;
	}
	
	private List<Usuario> findByEmail(String email) {
		String sql = "SELECT * FROM USUARIO"
				+ " where email = '"+email+"'";
		
		List<Usuario> usuarios = new ArrayList<>();
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		

		for (Map row : rows) {
			Usuario obj = new Usuario.UsuarioBuilder(
								(Integer)row.get("ID"),
								(String)row.get("email")			                
					        ).nombre((String)row.get("nombre"))
					        .apellidoPaterno((String)row.get("apellido_paterno"))
					        .rol((int)row.get("rol"))
					        .build();
			

			usuarios.add(obj);
		}
		
		return usuarios;
	}

	@Override
	public Usuario loginG(Usuario usuario) {
		List<Usuario> uT = findByEmail(usuario.getEmail());
		if( uT.isEmpty() ) {
			save(usuario);
		}else {
			usuario.setId(uT.get(0).getId());
		}
		
		return findByEmail(usuario.getEmail()).get(0);
	}

	@Override
	public boolean validateUsuarioRegistrado(String usuario) {
		String sql = "SELECT * FROM USUARIO"
				+ " where usuario ='"+usuario+"'";
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		

		for (Map row : rows) {
			return false;
		}
		return true;
	}

	@Override
	public boolean validateEmailRegistrado(String usuario) {
		String sql = "SELECT * FROM USUARIO"
				+ " where email ='"+usuario+"'";
		
		List<Usuario> usuarios = new ArrayList<>();
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		

		for (Map row : rows) {
			return false;
		}
		return true;
	}

	@Override
	public List<Usuario> getByIdProyecto(String idProyecto) {
		String sql = "SELECT * FROM USUARIO us"
					+ " inner join usuario_proyecto up on up.id_usuario = us.id"
					+ " where up.id_proyecto='"+idProyecto+"'";
		
		List<Usuario> usuarios = new ArrayList<>();
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		

		for (Map row : rows) {
			Usuario obj = new Usuario.UsuarioBuilder((Integer) row.get("ID"), (String) row.get("email"))
					.usuario((String) row.get("usuario"))
					.nombre((String) row.get("nombre"))
					.apellidoPaterno((String) row.get("apellido_paterno"))
					.apellidoMaterno((String) row.get("apellido_materno"))
					.contrasena((String) row.get("contrasena"))
					.build();
	
			usuarios.add(obj);
		}
		
		
		return usuarios;
	}

	@Override
	@Transactional
	public Usuario registerShare(Usuario usuario) {
		List<Usuario> uT = findByEmail(usuario.getEmail());
		if( uT.isEmpty() ) {
			save(usuario);
			usuario.setId(findByEmail(usuario.getEmail()).get(0).getId());
		}else {
			usuario.setId(uT.get(0).getId());
		}
	
		/*Verificar si ya se ha registrado*/
		
		String sql = "SELECT * FROM usuario_proyecto"
				+ " where id_proyecto ='"+usuario.getIdProyecto()+"' and id_usuario ='"+usuario.getId()+"'";

		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		
		boolean existe = false;
		for (Map row : rows) {
			existe =  true;
			break;
		}
		
		if(!existe) {
			String sql2 = "insert into usuario_proyecto (id_usuario, id_proyecto) "
					+ "values (?, ?)";
			jdbcTemplate.update(sql2, usuario.getId(), usuario.getIdProyecto());   
		}
		

		
		
		return findByEmail(usuario.getEmail()).get(0);
		/*Linkear proyecto a proyectos share*/
		
	
	}

	
}
