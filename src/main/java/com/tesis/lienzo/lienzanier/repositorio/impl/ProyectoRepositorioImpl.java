package com.tesis.lienzo.lienzanier.repositorio.impl;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tesis.lienzo.lienzanier.bean.Indicador;
import com.tesis.lienzo.lienzanier.bean.Metrica;
import com.tesis.lienzo.lienzanier.bean.Proyecto;
import com.tesis.lienzo.lienzanier.bean.UsuarioProyecto;
import com.tesis.lienzo.lienzanier.repositorio.IndicadorRepositorio;
import com.tesis.lienzo.lienzanier.repositorio.MetricaRepositorio;
import com.tesis.lienzo.lienzanier.repositorio.ProyectoRepositorio;

@Repository
public class ProyectoRepositorioImpl implements ProyectoRepositorio{

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private MetricaRepositorio metricaRepositorio;
	
	@Autowired
	private IndicadorRepositorio indicadorRepositorio;
	
	@Override
	public List<Proyecto> getAll() {
		// TODO Auto-generated method stub
		String sql = "SELECT * FROM PROYECTO";
		
		List<Proyecto> proyectos = new ArrayList<>();
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		

		for (Map row : rows) {
			Proyecto obj = new Proyecto();
			obj.setId((Integer) row.get("ID"));
			obj.setNombre((String) row.get("nombre"));
			obj.setDescripcion((String) row.get("descripcion"));

			proyectos.add(obj);
		}
		
		
		return proyectos;
	}

	@Transactional
	@Override
	public String save(Proyecto proyecto) {
		KeyHolder holder = new GeneratedKeyHolder();
		

		String sql = "insert into proyecto (nombre, descripcion, tipo, estado) "
				+ "values (?, ?, ?, 1)";
		int r = jdbcTemplate.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, proyecto.getNombre());
				ps.setString(2, proyecto.getDescripcion());
				ps.setInt(3, proyecto.getTipo());
				return ps;
			}
		}, holder);

		int newProyectoId = (int) holder.getKeys().get("id");
		
		/*Insertar batch usuarios_proyecto*/
		List<UsuarioProyecto> usuariosXproyecto = proyecto.getUsuarioXproyecto();
		int[] updateCounts = jdbcTemplate.batchUpdate(
                "insert into usuario_proyecto (id_usuario, id_proyecto) "
                + "values (?, ?)",
                new BatchPreparedStatementSetter() {
                    public void setValues(PreparedStatement ps, int i) throws SQLException {
                        ps.setInt(1, usuariosXproyecto.get(i).getId_usuario());
                        ps.setInt(2, newProyectoId);
                    }

                    public int getBatchSize() {
                        return usuariosXproyecto.size();
                    }
                } );
		
		saveProyectoArea(newProyectoId);
		saveMetricasDefault(newProyectoId);
		if(r == 1)
			return "OK";
		return "NO";
	}

	private void saveProyectoArea(int newProyectoId) {
		/*Insertar batch usuarios_proyecto*/
		int[] updateCounts = jdbcTemplate.batchUpdate(
                "insert into proyecto_area (id_proyecto, id_area) "
                + "values (?, ?)",
                new BatchPreparedStatementSetter() {
                    public void setValues(PreparedStatement ps, int i) throws SQLException {
                        ps.setInt(1, newProyectoId);
                        ps.setInt(2, i+1);
                    }

                    public int getBatchSize() {
                        return 9;
                    }
                } );
		
	}
	
	private void saveMetricasDefault(int newProyectoId) {
		Metrica totalIngresos = new Metrica();
		totalIngresos.setIdProyecto(newProyectoId);
		totalIngresos.setNombre("Total de ingresos");
		totalIngresos.setNombreCorto("Total de ingresos");
		totalIngresos.setDescripcion("Total de ingresos");
		
		int idTotalIngresos = metricaRepositorio.save(totalIngresos);
		totalIngresos.setId(idTotalIngresos);
		
		Metrica numeroOrdenes = new Metrica();
		numeroOrdenes.setIdProyecto(newProyectoId);
		numeroOrdenes.setNombre("Número de órdenes");
		numeroOrdenes.setNombreCorto("Número de órdenes");
		numeroOrdenes.setDescripcion("Número de órdenes");
		
		int idNumeroOrdenes = metricaRepositorio.save(numeroOrdenes);
		numeroOrdenes.setId(idNumeroOrdenes);
		
		//insertar Average Order Value 
		Indicador indicadorAverage = new Indicador();
		
		indicadorAverage.setIdProyecto(newProyectoId);
		indicadorAverage.setNombre("Average Order Value");
		indicadorAverage.setDescripcion("Se refiere al valor medio del carrito de compra de los clientes de una tienda online. Es decir, informa acerca de la cantidad de dinero que gastan los usuarios como promedio en nuestro ecommerce.");
		indicadorAverage.setTxt_indicador("L"+idTotalIngresos+"/L"+idNumeroOrdenes);
		
		indicadorAverage.getMetricasInt().add(idTotalIngresos);
		indicadorAverage.getMetricasInt().add(idNumeroOrdenes);
		
		indicadorRepositorio.save(indicadorAverage);
		
		/////////////////////////////////////////////////////////////////
		
		Metrica sumaInversiones = new Metrica();
		sumaInversiones.setIdProyecto(newProyectoId);
		sumaInversiones.setNombre("Suma de inversiones");
		sumaInversiones.setNombreCorto("Suma de inversiones");
		sumaInversiones.setDescripcion("Suma de inversiones");
		
		int idsumaInversiones = metricaRepositorio.save(sumaInversiones);
		sumaInversiones.setId(idsumaInversiones);
		
		
		Metrica clientesAdquiridos = new Metrica();
		clientesAdquiridos.setIdProyecto(newProyectoId);
		clientesAdquiridos.setNombre("Número de clientes adquiridos");
		clientesAdquiridos.setNombreCorto("Número de clientes adquiridos");
		clientesAdquiridos.setDescripcion("Número de clientes adquiridoss");
		
		int idclientesAdquiridos = metricaRepositorio.save(clientesAdquiridos);
		clientesAdquiridos.setId(idclientesAdquiridos);
		
		//insertar Customer Acquisition Cost 
		Indicador indicadorCac = new Indicador();
		indicadorCac.setIdProyecto(newProyectoId);
		indicadorCac.setNombre("Customer Acquisition Cost");
		indicadorCac.setDescripcion("Se refiere a la inversión que se realiza para obtener un nuevo cliente. Se trata de una métrica unitaria que se basa en la media por cada nuevo cliente, ayudando a realizar extrapolaciones que nos servirán para comprender mejor el funcionamiento del negocio."
				+ "        Para calcular el monto invertido se debe considerar únicamente aquellos involucrados directamente en la adquisición de clientes, generalmente las áreas de Marketing y Ventas. No se considera costos de administración, SAC o desarrollo del producto."
				+ "        De igual forma, para calcular el número de clientes solo considerar aquellos que hayan sido por la estrategia tomada. Por ejemplo, si en un blog mencionan a tu producto y obtienes algún cliente por este medio, no se debería considerar porque no invertiste ningún esfuerzo o dinero en ello.");
		indicadorCac.setTxt_indicador("L"+idsumaInversiones+"/L"+idclientesAdquiridos);
		
		indicadorCac.getMetricasInt().add(idsumaInversiones);
		indicadorCac.getMetricasInt().add(idclientesAdquiridos);
		
		indicadorRepositorio.save(indicadorCac);
		
		//////////////////////////////////////////////////////////////////////////////////
		Metrica gastoPromedio = new Metrica();
		gastoPromedio.setIdProyecto(newProyectoId);
		gastoPromedio.setNombre("Gasto promedio de un cliente");
		gastoPromedio.setNombreCorto("Gasto promedio de un cliente");
		gastoPromedio.setDescripcion("Gasto promedio de un cliente");
		
		int idgastoPromedio = metricaRepositorio.save(gastoPromedio);
		gastoPromedio.setId(idgastoPromedio);
		
		Metrica vecesCompra = new Metrica();
		vecesCompra.setIdProyecto(newProyectoId);
		vecesCompra.setNombre("Número de veces promedio de compra por cliente");
		vecesCompra.setNombreCorto("Número de veces promedio de compra por cliente");
		vecesCompra.setDescripcion("Número de veces promedio de compra por cliente");
		
		int idvecesCompra = metricaRepositorio.save(vecesCompra);
		vecesCompra.setId(idvecesCompra);
		
		Metrica tiempoRelacion = new Metrica();
		tiempoRelacion.setIdProyecto(newProyectoId);
		tiempoRelacion.setNombre("Tiempo promedio de relacion, en años");
		tiempoRelacion.setNombreCorto("Tiempo promedio de relacion, en años");
		tiempoRelacion.setDescripcion("Tiempo promedio de relacion, en años");
		
		int idtiempoRelacion = metricaRepositorio.save(tiempoRelacion);
		tiempoRelacion.setId(idtiempoRelacion);
		
		
		//insertar LifeTime Value 
		Indicador indicadorLife = new Indicador();
		indicadorLife.setIdProyecto(newProyectoId);
		indicadorLife.setNombre("LifeTime Value");
		indicadorLife.setDescripcion("Es el término que se utiliza para determinar el valor que un cliente aporta a un negocio durante toda la vida útil de la empresa."
				+ "        Es el valor neto de los ingresos que nos genera un cliente durante el tiempo que es nuestro cliente."
				+ "        El CAC debe ser menor que el LTV, de lo contrario se estaría inviertiendo más en captar un cliente que en valor que este tiene una vez captado");
		
		indicadorLife.setTxt_indicador("L"+idgastoPromedio+"*L"+idvecesCompra+"*L"+idtiempoRelacion);
		
		indicadorLife.getMetricasInt().add(idgastoPromedio);
		indicadorLife.getMetricasInt().add(idvecesCompra);
		indicadorLife.getMetricasInt().add(idtiempoRelacion);
		
		indicadorRepositorio.save(indicadorLife);
		
		//////////////////////////////////////////////////////////////////////////////
		Metrica clientesFinales = new Metrica();
		clientesFinales.setIdProyecto(newProyectoId);
		clientesFinales.setNombre("Clientes terminar el periodo");
		clientesFinales.setNombreCorto("Clientes terminar el periodo");
		clientesFinales.setDescripcion("Clientes terminar el periodo");
		
		int idclientesFinales = metricaRepositorio.save(clientesFinales);
		clientesFinales.setId(idclientesFinales);
		
		Metrica nuevosClientes = new Metrica();
		nuevosClientes.setIdProyecto(newProyectoId);
		nuevosClientes.setNombre("nuevos clientes");
		nuevosClientes.setNombreCorto("nuevos clientes");
		nuevosClientes.setDescripcion("nuevos clientes");
		
		int idnuevosClientes = metricaRepositorio.save(nuevosClientes);
		nuevosClientes.setId(idnuevosClientes);
		
		Metrica clientesIniciales = new Metrica();
		clientesIniciales.setIdProyecto(newProyectoId);
		clientesIniciales.setNombre("Clientes al inicial periodo");
		clientesIniciales.setNombreCorto("Clientes al inicial periodo");
		clientesIniciales.setDescripcion("Clientes al inicial periodo");
		
		int idclientesIniciales = metricaRepositorio.save(clientesIniciales);
		clientesIniciales.setId(idclientesIniciales);
		
		//insertar Tasa de retención
		Indicador indicadorRetencion = new Indicador();
		indicadorRetencion.setIdProyecto(newProyectoId);
		indicadorRetencion.setNombre("Tasa de retención");
		indicadorRetencion.setDescripcion("La tasa de retención de clientes es el índice que mide la fidelidad de los clientes hacia un negocio durante un plazo de tiempo concreto, expresado en un porcentaje."
				+ "        Es decir, que indica la capacidad de un negocio para retener a sus clientes durante un tiempo determinado."
				+ "        Si la tasa de retención es alta: los clientes son fieles y compran repetidamente en el mismo e-commerce."
				+ "        Si la tasa es baja: suelen entrar clientes nuevos, pero muy pocos repiten compra.");
		
		indicadorRetencion.setTxt_indicador("(L"+idclientesFinales+"-L"+idnuevosClientes+")/L"+idclientesIniciales);
		
		indicadorRetencion.getMetricasInt().add(idclientesFinales);
		indicadorRetencion.getMetricasInt().add(idnuevosClientes);
		indicadorRetencion.getMetricasInt().add(idclientesIniciales);
		
		indicadorRepositorio.save(indicadorRetencion);
		
		//insertar Churn Rate
		Indicador indicadorChurn = new Indicador();
		indicadorChurn.setIdProyecto(newProyectoId);
		indicadorChurn.setNombre("Churn Rate");
		indicadorChurn.setDescripcion("Es la tasa de cancelación o pérdida de usuarios durante un período determinado de tiempo. Se refiere a la cantidad de clientes o suscriptores que un negocio o base de datos deja de tener durante el período de tiempo que se quiera estudiar, y se expresa en porcentaje."
				+ "        El churn rate es una métrica que a cualquier negocio basado en la suscripción (por ejemplo, una compañía de seguros o la mencionada herramienta online mediante suscripción) o con bases de datos de clientes o listas de correo (por ejemplo, aquellas a las que enviamos nuestras campañas de email marketing) le conviene controlar, porque indica el porcentaje de clientes que han perdido el interés en aquello que les ofrecemos, ya sea un producto, un servicio o un contenido.");
		
		indicadorChurn.setTxt_indicador("(L"+idclientesIniciales+"-L"+idclientesFinales+")/L"+idclientesIniciales);
		
		indicadorChurn.getMetricasInt().add(idclientesIniciales);
		indicadorChurn.getMetricasInt().add(idclientesFinales);
		
		indicadorRepositorio.save(indicadorChurn);
		
		
		


	}

	@Override
	public List<Proyecto> getByUser(int idUsuario) {
		String sql = "select * from proyecto pr"
				+ " inner join usuario_proyecto up on up.id_usuario=?"
				+ " and pr.id = up.id_proyecto and pr.estado=1";
		
		List<Proyecto> proyectos = new ArrayList<>();
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql,new Object[]{idUsuario});
		

		for (Map row : rows) {
			Proyecto obj = new Proyecto();
			obj.setId((Integer) row.get("ID"));
			obj.setNombre((String) row.get("nombre"));
			obj.setDescripcion((String) row.get("descripcion"));

			proyectos.add(obj);
		}
		
		
		return proyectos;
	}

	@Override
	public void deleteById(int id) {
		String sql = "update proyecto"
				+ "	set estado = '0'"
				+ " where id = ?";
		jdbcTemplate.update(sql, id);
	}

	@Override
	public void update(Proyecto proyecto) {
		String sql = "update proyecto"
				+ "	set nombre = ?, descripcion = ?"
				+ " where id = ?";
		jdbcTemplate.update(sql, proyecto.getNombre(), proyecto.getDescripcion(), proyecto.getId());
		
	}

	@Override
	public Proyecto getById(String id) {
		// TODO Auto-generated method stub
		String sql = "SELECT * FROM PROYECTO where id='"+id+"'";
		
		
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		

		for (Map row : rows) {
			Proyecto obj = new Proyecto();
			obj.setId((Integer) row.get("ID"));
			obj.setNombre((String) row.get("nombre"));
			obj.setDescripcion((String) row.get("descripcion"));
			obj.setTipo((Integer) row.get("tipo"));
			return obj;
		}
		
		return null;
	
	}

}
