package com.tesis.lienzo.lienzanier.repositorio;

import java.util.List;

import com.tesis.lienzo.lienzanier.bean.Usuario;

public interface UsuarioRepositorio {

	List<Usuario> findAll();
	
	String save(Usuario usuario);
	
	Usuario login(Usuario usuario);
	
	List<Usuario> findByEmail(String email, Integer idUser);
	
	Usuario loginG(Usuario usuario);

	boolean validateUsuarioRegistrado(String usuario);

	boolean validateEmailRegistrado(String usuario);

	List<Usuario> getByIdProyecto(String idProyecto);

	Usuario registerShare(Usuario usuario);
}
