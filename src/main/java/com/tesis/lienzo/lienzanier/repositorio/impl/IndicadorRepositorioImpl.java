package com.tesis.lienzo.lienzanier.repositorio.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tesis.lienzo.lienzanier.bean.Comentario;
import com.tesis.lienzo.lienzanier.bean.Condicion;
import com.tesis.lienzo.lienzanier.bean.Dia;
import com.tesis.lienzo.lienzanier.bean.DiaIndicador;
import com.tesis.lienzo.lienzanier.bean.Experimento;
import com.tesis.lienzo.lienzanier.bean.Fecha;
import com.tesis.lienzo.lienzanier.bean.Indicador;
import com.tesis.lienzo.lienzanier.bean.MesIndicador;
import com.tesis.lienzo.lienzanier.bean.Metrica;
import com.tesis.lienzo.lienzanier.bean.PuntoMedicion;
import com.tesis.lienzo.lienzanier.bean.PuntoMedicionIndicador;
import com.tesis.lienzo.lienzanier.repositorio.IndicadorRepositorio;
import com.tesis.lienzo.lienzanier.util.Constantes;

@Repository
public class IndicadorRepositorioImpl implements IndicadorRepositorio{

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Override
	public void save(Indicador indicador) {
		// TODO Auto-generated method stub
		KeyHolder holder = new GeneratedKeyHolder();
		
		String sql = "insert into indicador (id_proyecto, nombre, descripcion, txt_indicador) "
				+ "values (?, ?, ?, ?)";
		int r = jdbcTemplate.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1, indicador.getIdProyecto());
				ps.setString(2, indicador.getNombre());
				ps.setString(3, indicador.getDescripcion());
				ps.setString(4, indicador.getTxt_indicador());
				return ps;
			}
		}, holder);
		
		int newIndicadorId = (int) holder.getKeys().get("id");
		
		
		/*Insertar en metricas_indicador*/
		/*Save metrica_experimento*/
		List<Integer> idMetricas = indicador.getMetricasInt();
		int[] updateCounts = jdbcTemplate.batchUpdate(
                "insert into metrica_indicador (id_metrica, id_indicador) "
                + "values (?, ?)",
                new BatchPreparedStatementSetter() {
                    public void setValues(PreparedStatement ps, int i) throws SQLException {
                        ps.setInt(1, idMetricas.get(i));
                        ps.setInt(2, newIndicadorId);
                    }

                    public int getBatchSize() {
                        return idMetricas.size();
                    }
                } );
		
	}

	@Override
	public List<Indicador> listar(String idProyecto) {
		
		
		
		String sql2 = "select * from indicador"
				+ " where id_proyecto='"+idProyecto+"'";
		
		List<Indicador> indicadores = new ArrayList<>();
		List<Map<String, Object>> rows2 = jdbcTemplate.queryForList(sql2);
		

		for (Map row : rows2) {
			Indicador obj = new Indicador();
			obj.setId((Integer)row.get("ID"));
			obj.setIdProyecto((Integer)row.get("id_proyecto"));
			obj.setNombre((String)row.get("nombre"));
			obj.setDescripcion((String)row.get("descripcion"));
			obj.setTxt_indicador((String)row.get("txt_indicador"));
			
			
			String sql = "select me.id as id, me.nombre as nombre, me.nombre_corto as corto from metrica me"
					+ " inner join metrica_indicador mei on mei.id_metrica = me.id"
					+ " where mei.id_indicador='"+obj.getId()+"'";
					
			
			List<Metrica> metricas = new ArrayList<>();
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
			

			for (Map row2 : rows) {
				Metrica obj2 = new Metrica();
				obj2.setId((Integer)row2.get("id"));
				obj2.setNombre((String)row2.get("nombre"));
				obj2.setNombreCorto((String)row2.get("corto"));
				metricas.add(obj2);
			}
			
			obj.setMetricas(metricas);
			indicadores.add(obj);
		}
	
		return indicadores;
	}

	@Override
	public Indicador getIndicador(String idIndicador) {
		
		
		String sql = "select * from indicador"
				+ " where id='"+idIndicador+"'";
		
		List<Indicador> indicadores = new ArrayList<>();
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		
		for (Map row : rows) {
			Indicador obj = new Indicador();
			obj.setId((Integer)row.get("id"));
			obj.setNombre((String)row.get("nombre"));
			obj.setDescripcion((String)row.get("descripcion"));
			obj.setTxt_indicador((String)row.get("txt_indicador"));
			indicadores.add(obj);
		}
		
		Indicador indicadorResponse = indicadores.get(0);
		
		
		/*Obtenemos Fechas*/
		String sql4 = "select * from mes_indicador"
				+	" where id_indicador='"+idIndicador+"' order by mes";
		
		List<MesIndicador> fechas = new ArrayList<>();
		List<Map<String, Object>> rows4 = jdbcTemplate.queryForList(sql4);
		

		for (Map row : rows4) {
			MesIndicador obj = new MesIndicador();
			obj.setId((Integer)row.get("ID"));
			obj.setIdIndicador(Integer.valueOf(idIndicador));
			obj.setMes((Integer)row.get("mes"));
			
			String sql5 = "select * from dia_indicador"
					+ " where id_mes='"+obj.getId()+"' order by dia";
			List<DiaIndicador> dias = new ArrayList<>();
			List<Map<String, Object>> rows5 = jdbcTemplate.queryForList(sql5);
			for (Map row5 : rows5) {
				DiaIndicador dia = new DiaIndicador();
				dia.setId((Integer)row5.get("id"));
				dia.setIdMes((Integer)row5.get("id_mes"));
				dia.setDia((Integer)row5.get("dia"));
				dia.setSemana((Integer)row5.get("semana"));
				
				/*Obtener punto medicion*/
				String sql3 = "select pm.id as idPuntoMedicion, me.id as idMetricaIndicador, pm.valor, met.nombre_corto as nombreCorto, met.id as idMetrica from punto_medicion_indicador pm"
						+ " inner join metrica_indicador me on me.id = pm.id_metrica_indicador"
						+ " inner join metrica met on met.id = me.id_metrica"
						+ " where pm.id_dia='"+dia.getId()+"'";
				
				List<PuntoMedicionIndicador> mediciones = new ArrayList<>();
				List<Map<String, Object>> rows3 = jdbcTemplate.queryForList(sql3);
				

				for (Map row3 : rows3) {
					PuntoMedicionIndicador obj3 = new PuntoMedicionIndicador();
					obj3.setId((Integer)row3.get("idpuntomedicion"));
					obj3.setIdMetricaIndicador((Integer)row3.get("idMetricaIndicador"));
					obj3.setMetrica((String)row3.get("nombrecorto"));
					obj3.setValor((Integer)row3.get("valor"));
					obj3.setIdMetrica((Integer)row3.get("idmetrica"));
				    obj3.setIdentificador("L"+obj3.getIdMetrica());
					mediciones.add(obj3);
				}
				
				dia.setPuntos(mediciones);
				dias.add(dia);
			}
			obj.setDias(dias);
			
			

			fechas.add(obj);
		}
		
		indicadorResponse.setMeses(fechas);

		
		/*Metricas / Indicadores*/
		String sql5 = "select me.id as id, me.nombre as nombre, me.nombre_corto as corto from metrica me"
				+ " inner join metrica_indicador mei on mei.id_metrica = me.id"
				+ " where mei.id_indicador='"+idIndicador+"'";
		
		List<Metrica> metricas = new ArrayList<>();
		List<Map<String, Object>> rows5 = jdbcTemplate.queryForList(sql5);
		

		for (Map row : rows5) {
			Metrica obj = new Metrica();
			obj.setId((Integer)row.get("ID"));
			obj.setNombre((String)row.get("nombre"));
			obj.setNombreCorto((String)row.get("corto"));
		
			metricas.add(obj);
		}
		
		indicadorResponse.setMetricas(metricas);
		
		
		return indicadorResponse;
		
		
		
		
		
	}

	@Transactional
	@Override
	public void saveMediciones(Indicador indicador) {
		// TODO Auto-generated method stub
		
		int idIndicador = indicador.getId();
		for(MesIndicador fe : indicador.getMeses()) {
			KeyHolder holder = new GeneratedKeyHolder();
			
			String sql = "insert into mes_indicador (id_indicador, mes) "
					+ "values (?, ?)";
			int r = jdbcTemplate.update(new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
					PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
					ps.setInt(1, idIndicador);
					ps.setInt(2, fe.getMes());
					return ps;
				}
			}, holder);
			
			int newFechaId = (int) holder.getKeys().get("id");
			
			List<DiaIndicador> dias = fe.getDias();
			for(DiaIndicador dia: dias) {
				KeyHolder holder3 = new GeneratedKeyHolder();
				String sql3 = "insert into dia_indicador (id_mes, dia, semana) "
						+ "values (?, ?, ?)";
				int r3 = jdbcTemplate.update(new PreparedStatementCreator() {
					@Override
					public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
						PreparedStatement ps = connection.prepareStatement(sql3, Statement.RETURN_GENERATED_KEYS);
						ps.setInt(1, newFechaId);
						ps.setInt(2, dia.getDia());
						ps.setInt(3, dia.getSemana());
						return ps;
					}
				}, holder3);
				
				int newDiaId = (int) holder3.getKeys().get("id");
				
				/*Guardamos punto_medicion*/
				List<PuntoMedicionIndicador> mediciones = dia.getPuntos();
				for(PuntoMedicionIndicador pm : mediciones) {
					int idMetricaIndicador = getIdMetricaIndicador(pm.getIdMetrica(), idIndicador);
					pm.setIdMetricaIndicador(idMetricaIndicador);
				}
				
				int[] updateCounts = jdbcTemplate.batchUpdate(
		                "insert into punto_medicion_indicador (id_dia, id_metrica_indicador, valor) "
		                + "values (?, ?, ?)",
		                new BatchPreparedStatementSetter() {
		                    public void setValues(PreparedStatement ps, int i) throws SQLException {
		                        ps.setInt(1, newDiaId);
		                        ps.setInt(2, mediciones.get(i).getIdMetricaIndicador());
		                        ps.setInt(3, mediciones.get(i).getValor());
		                    }

		                    public int getBatchSize() {
		                        return mediciones.size();
		                    }
		                } );
				
				
				
			}
			
			
			
			
		}
		
		
		
	}

	@Override
	public void deleteFechaAndMediciones(Indicador indicador) {
		// TODO Auto-generated method stub
		String sql = "delete from punto_medicion_indicador"
				+ " using mes_indicador, indicador, dia_indicador"
				+ " where punto_medicion_indicador.id_dia = dia_indicador.id"
				+ " and mes_indicador.id = dia_indicador.id_mes"
				+ "	and indicador.id = mes_indicador.id_indicador"
				+ "	and indicador.id='"+indicador.getId()+"'";
		jdbcTemplate.update(sql);
		
		String sq3 = "delete from dia_indicador"
				+ " using mes_indicador, indicador"
				+ " where mes_indicador.id = dia_indicador.id_mes"
				+ "	and indicador.id = mes_indicador.id_indicador"
				+ "	and indicador.id='"+indicador.getId()+"'";
		jdbcTemplate.update(sq3);
		
		String sql2 = "delete from mes_indicador"
				+ " where id_indicador='"+indicador.getId()+"'";
		jdbcTemplate.update(sql2);

	}
	
	private int getIdMetricaIndicador(int idMetrica, int idIndicador) {
		String sql = "select * from metrica_indicador"
				+ " where id_metrica="+idMetrica+ " and id_indicador="+idIndicador;
		
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		

		for (Map row : rows) {
			return (Integer)row.get("ID");
			
		}
		
		return 0;
	}

}
