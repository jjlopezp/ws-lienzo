package com.tesis.lienzo.lienzanier.repositorio;

import java.util.List;

import com.tesis.lienzo.lienzanier.bean.Proyecto;

public interface ProyectoRepositorio {

	public List<Proyecto> getAll();
	public List<Proyecto> getByUser(int id);
	public String save(Proyecto proyecto);
	void deleteById(int id);
	void update(Proyecto proyecto);
	public Proyecto getById(String id);
}
