package com.tesis.lienzo.lienzanier.repositorio;


import java.util.List;

import com.tesis.lienzo.lienzanier.bean.Indicador;

public interface IndicadorRepositorio {

	void save(Indicador indicador);

	List<Indicador> listar(String idProyecto);

	Indicador getIndicador(String idIndicador);

	void saveMediciones(Indicador indicador);

	void deleteFechaAndMediciones(Indicador indicador);

}
