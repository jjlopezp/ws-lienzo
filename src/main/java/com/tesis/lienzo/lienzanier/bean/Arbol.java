package com.tesis.lienzo.lienzanier.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Arbol {

	private NodoArbol root;
	
	public Arbol() {};
	
	public Map<Integer, Arbol> construirArbol(List<Relacion> relaciones){
		
		/*Se crea 2 listas
		 * Una con cada uno de los nodos(hipotesis)
		 * Otra con los nodos cabeza*/
		
		Map<Integer, NodoArbol> hipotesis = new HashMap<>();
		Map<Integer, Arbol> cabezas = new HashMap<>();
		
		for(Relacion rel: relaciones) {
			
			int posPadre= rel.getIdPadre();
			int posHijo= rel.getIdHijo();
			
			if(hipotesis.get(posPadre) == null) {
				hipotesis.put(posPadre, new NodoArbol(posPadre)); //posHijo es el mismo id ahora
				
				cabezas.put(posPadre, new Arbol(hipotesis.get(posPadre)));
			}
			
			if(posHijo != 0) {
				if(hipotesis.get(posHijo) == null) {
					hipotesis.put(posHijo, new NodoArbol(posHijo));
				}else {
					cabezas.put(posHijo, null);
				}
				
				hipotesis.get(posPadre).agregarHijo(hipotesis.get(posHijo), posHijo);
			}
			
			
			
		}
		
		return cabezas;
		
	}
	
	public Arbol(NodoArbol nodo) {
		this.root = nodo;
	}
	
}
