package com.tesis.lienzo.lienzanier.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Comentario {
	
	private int id;
	private int idProyecto;
	private int idExperimento;
	private String suposicion;
	private String comentario;
	private int estadoExperimento;

}
