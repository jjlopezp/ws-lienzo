package com.tesis.lienzo.lienzanier.bean;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Hipotesis {

	private int id;
	private String suposicion;
	private String publico;
	private int duracion;
	private String descripcion;
	private int estado;
	private int idExperimento;
	private String fechaRegistro;
}
