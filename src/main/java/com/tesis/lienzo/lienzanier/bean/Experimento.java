package com.tesis.lienzo.lienzanier.bean;

import java.util.List;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Experimento {

	private int id;
	private int idHipotesis;
	private String hipotesis;
	private String descripcion;
	private int estado;
	private String txtEstado;
	private String tipoPrueba;
	private List<Metrica> metricas;
	private List<Condicion> condiciones;
	
	private List<Fecha> fechas;
	private String evidencias;
}
