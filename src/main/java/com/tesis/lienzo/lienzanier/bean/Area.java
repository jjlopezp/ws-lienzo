package com.tesis.lienzo.lienzanier.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Area {
	
	private int id;
	private String nombre;
	private String descripcion;

}
