package com.tesis.lienzo.lienzanier.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.tesis.lienzo.lienzanier.service.LienzoService;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class NodoArbol {
	

	List<NodoArbol> children;
	Map<Integer, NodoArbol>hijos;
	int id;
	String suposicion;
	String conclu;
	String description;
	String name;
	
	public void agregarHijo(NodoArbol n, int index) {
		this.hijos.put(index, n);
	}
	
	public NodoArbol() {};
	
	public NodoArbol(int i) {
		this.id = i;
		this.hijos = new HashMap<>();
		this.children = new ArrayList<>();
		this.name = "Hipotesis";
	}
	
	public void printTreeIdented(LienzoService lienzoService) {		 
		
		lienzoService.fillNodoHipotesis(this);
	
		for (Map.Entry<Integer, NodoArbol> entry : hijos.entrySet()) {
			NodoArbol hijo = entry.getValue();
            if (hijo != null) {
            	
            	this.getChildren().add(hijo);
            	hijo.printTreeIdented(lienzoService);
            }
            	 
		}		 
	}
	
}
