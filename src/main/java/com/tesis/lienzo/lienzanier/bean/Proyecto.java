package com.tesis.lienzo.lienzanier.bean;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Proyecto {

	private Integer id;
	private String nombre;
	private String descripcion;
	private List<UsuarioProyecto> usuarioXproyecto;
	private List<ProyectoArea> areas;
	private int tipo;
	private Date fecha_reg;
	private Date fecha_mod;
	
}
