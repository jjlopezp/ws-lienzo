package com.tesis.lienzo.lienzanier.bean;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MesIndicador {

	private int id;
	private int idIndicador;
	private int mes;
	private List<DiaIndicador> dias;
}
