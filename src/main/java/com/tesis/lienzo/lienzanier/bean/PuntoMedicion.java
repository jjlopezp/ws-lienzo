package com.tesis.lienzo.lienzanier.bean;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PuntoMedicion {

	private int id;
	private int idMetricaExperimento;
	private int idMetrica;
	private String metrica;
	private int valor;
	private String identificador;
	
	
}