package com.tesis.lienzo.lienzanier.bean;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Fecha {

	private int id;
	private String fecha;
	private int mes;
	private List<Dia> dias;
}
