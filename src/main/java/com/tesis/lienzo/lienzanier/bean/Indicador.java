package com.tesis.lienzo.lienzanier.bean;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Indicador {

	private int id;
	private int idProyecto;
	private String nombre;
	private String descripcion;
	private String txt_indicador;
	private List<MesIndicador> meses;
	private List<Metrica> metricas;
	private List<Integer> metricasInt;
	
	public  Indicador() {
		this.metricasInt = new ArrayList<>();
	}
}
