package com.tesis.lienzo.lienzanier.bean;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Dia {
	private int id;
	private int id_fecha;
	private int dia;
	private int semana;
	private List<PuntoMedicion> mediciones;
}
