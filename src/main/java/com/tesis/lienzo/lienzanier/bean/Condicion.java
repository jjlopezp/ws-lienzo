package com.tesis.lienzo.lienzanier.bean;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Condicion {

	private int id;
	private int idExperimento;
	private String jsonTipo;
	private String jsonValor;
	private String txtCondicion;
	private String descripcion;
}
