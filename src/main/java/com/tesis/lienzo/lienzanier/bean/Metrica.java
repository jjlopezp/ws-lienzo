package com.tesis.lienzo.lienzanier.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Metrica {
	
	private int id;
	private int idProyecto;
	private String nombre;
	private String nombreCorto;
	private String descripcion;
	private String usuario;
	
	
}
