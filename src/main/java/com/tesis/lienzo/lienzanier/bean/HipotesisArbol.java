package com.tesis.lienzo.lienzanier.bean;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HipotesisArbol {
	
	private int id;
	private HipotesisArbol hipotesisPadre;
	private List<HipotesisArbol> hipotesisHijos;
	
	
}
