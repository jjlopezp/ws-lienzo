package com.tesis.lienzo.lienzanier.bean;

import java.io.Serializable;
import java.util.Date;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Usuario implements Serializable{
	
	
		/**
	 * 
	 */
		private static final long serialVersionUID = 1L;
		private Integer id;
		private String usuario;
		private String nombre;
		private String apellidoPaterno;
		private String apellidoMaterno;
		private String email;
		private String contrasena;
		private String telefono;
		private String linkedin;
		private int estado;
		private Date fecha_reg;
		private Date fecha_mod;
		private int rol;
		private int idProyecto;
		private String correoRevisor;
		private String urlShare;
		
		public Usuario() {};
		
		private Usuario(UsuarioBuilder builder) {
			this.id = builder.id;
			this.usuario = builder.usuario;
			this.nombre = builder.nombre;
			this.apellidoPaterno = builder.apellidoPaterno;
			this.apellidoMaterno = builder.apellidoMaterno;
			this.email = builder.email;
			this.contrasena = builder.contrasena;
			this.telefono = builder.telefono;
			this.linkedin = builder.linkedin;
			this.estado = builder.estado;
			this.rol = builder.rol;
			this.idProyecto = builder.idProyecto;
			this.correoRevisor = builder.correoRevisor;
			this.urlShare = builder.urlShare;
		}
		
		
		public static class UsuarioBuilder{
			private final Integer id;
			private String usuario;
			private String nombre;
			private String apellidoPaterno;
			private String apellidoMaterno;
			private final String email;
			private String contrasena;
			private String telefono;
			private String linkedin;
			private int estado;
			private int rol;
			private int idProyecto;
			private String correoRevisor;
			private String urlShare;
			
			public UsuarioBuilder(Integer id, String email) {
				this.id = id;
				this.email = email;
			}
			
			public UsuarioBuilder correoRevisor(String correoRevisor) {
				this.correoRevisor = correoRevisor;
				return this;
			}
			
			public UsuarioBuilder urlShare(String urlShare) {
				this.urlShare = urlShare;
				return this;
			}
			
			public UsuarioBuilder usuario(String usuario) {
				this.usuario = usuario;
				return this;
			}
			
			public UsuarioBuilder nombre(String nombre) {
				this.nombre = nombre;
				return this;
			}
			
			public UsuarioBuilder apellidoPaterno(String apellidoPaterno) {
				this.apellidoPaterno = apellidoPaterno;
				return this;
			}
			
			public UsuarioBuilder apellidoMaterno(String apellidoMaterno) {
				this.apellidoMaterno = apellidoMaterno;
				return this;
			}
			
			public UsuarioBuilder contrasena(String contrasena) {
				this.contrasena = contrasena;
				return this;
			}
			
			public UsuarioBuilder telefono(String telefono) {
				this.telefono = telefono;
				return this;
			}
			
			public UsuarioBuilder linkedin(String linkedin) {
				this.linkedin = linkedin;
				return this;
			}
			
			public UsuarioBuilder rol(int rol) {
				this.rol = rol;
				return this;
			}
			
			public UsuarioBuilder estado(Integer estado) {
				this.estado = estado;
				return this;
			}
			
			public UsuarioBuilder idProyecto(Integer idProyecto) {
				this.idProyecto = idProyecto;
				return this;
			}
			
			public Usuario build() {
				
				return new Usuario(this);
			}
		}
		
	
	
	
	
	
	
}
