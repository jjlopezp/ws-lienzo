package com.tesis.lienzo.lienzanier.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Relacion {

	private int idPadre;
	private int idHijo;
	
	public Relacion() {};
	
	public Relacion(int padre, int hijo) {
		this.idPadre = padre;
		this.idHijo = hijo;
	}
}
