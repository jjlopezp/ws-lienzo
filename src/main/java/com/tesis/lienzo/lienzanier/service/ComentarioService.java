package com.tesis.lienzo.lienzanier.service;

import java.util.List;

import com.tesis.lienzo.lienzanier.bean.Comentario;

public interface ComentarioService {

	public List<Comentario> getComentariosByProyecto(String idProyecto);

	public void updateEstadoComentario(String idComentario, String estado);

	public void saveComentarioProyecto(Comentario comentario);

	public List<Comentario> getComentariosByExperimento(String idExperimento);

	public void saveComentarioExperimento(Comentario comentario);

	public void updateEstadoComentarioExperimento(String idComentario, String estado);

	public List<Comentario> getComentariosDetalleByProyecto(String idProyecto);
}
