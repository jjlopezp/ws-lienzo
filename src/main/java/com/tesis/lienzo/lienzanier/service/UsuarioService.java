package com.tesis.lienzo.lienzanier.service;

import java.util.List;

import com.tesis.lienzo.lienzanier.bean.Usuario;

public interface UsuarioService {

	public List<Usuario> getAllUsuarios();
	
	public String save(Usuario usuario);
	
	public Usuario login(Usuario usuario);
	
	public List<Usuario> findByEmail(String email, String idUser);
	
	public Usuario loginG(Usuario usuario);

	public boolean validateUsuarioRegistrado(String usuario);

	public boolean validateEmailRegistrado(String usuario);

	public List<Usuario> getByIdProyecto(String idProyecto);

	public Usuario registerShare(Usuario usuario);
}
