package com.tesis.lienzo.lienzanier.service;

import java.util.List;

import com.tesis.lienzo.lienzanier.bean.Proyecto;

public interface ProyectoService {

	public List<Proyecto> getAll();
	public List<Proyecto> getByUser(String id);
	public String save(Proyecto proyecto);
	public void deleteById(String id);
	public void update(Proyecto proyecto);
	public Proyecto getById(String id);
}
