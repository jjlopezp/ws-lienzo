package com.tesis.lienzo.lienzanier.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tesis.lienzo.lienzanier.bean.Metrica;
import com.tesis.lienzo.lienzanier.repositorio.MetricaRepositorio;
import com.tesis.lienzo.lienzanier.service.MetricaService;

@Service
public class MetricaServiceImpl implements MetricaService{

	@Autowired
	MetricaRepositorio metricaRepositorio;
	
	@Override
	public List<Metrica> getMetricasByIdProyecto(String idProyecto) {
		// TODO Auto-generated method stub
		return metricaRepositorio.getMetricasByIdProyecto(idProyecto);
	}

	@Override
	public int save(Metrica proyecto) {
		// TODO Auto-generated method stub
		return metricaRepositorio.save(proyecto);
	}

	@Override
	public String update(Metrica proyecto) {
		// TODO Auto-generated method stub
		return metricaRepositorio.update(proyecto);
	}

	@Override
	public void delete(String id) {
		// TODO Auto-generated method stub
		metricaRepositorio.delete(id);
	}

}
