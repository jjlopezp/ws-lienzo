package com.tesis.lienzo.lienzanier.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tesis.lienzo.lienzanier.bean.Hipotesis;
import com.tesis.lienzo.lienzanier.bean.HipotesisArbol;
import com.tesis.lienzo.lienzanier.bean.NodoArbol;
import com.tesis.lienzo.lienzanier.bean.ProyectoArea;
import com.tesis.lienzo.lienzanier.bean.Relacion;
import com.tesis.lienzo.lienzanier.dto.HipotesisDto;
import com.tesis.lienzo.lienzanier.repositorio.LienzoRepositorio;
import com.tesis.lienzo.lienzanier.service.LienzoService;
import com.tesis.lienzo.lienzanier.util.Constantes;
import com.tesis.lienzo.lienzanier.util.MapperDtoToBean;

@Service
public class LienzoServiceImpl implements LienzoService{

	@Autowired
	LienzoRepositorio lienzoRepositorio;
	
	@Autowired
	MapperDtoToBean mapperDtoToBean;
	
	@Override
	public String save(HipotesisDto hipotesisDto) {
		
		return lienzoRepositorio.save(hipotesisDto);
	}

	@Override
	public List<ProyectoArea> getByProyecto(String idProyecto) {
	
		//Obtener lista de ProyectoArea
		List<ProyectoArea> proyectoAreas = lienzoRepositorio.getAreaByProyecto(idProyecto);
		
		for(ProyectoArea pYa: proyectoAreas) {
			List<Hipotesis> hipotesis= new ArrayList<>();
			hipotesis = lienzoRepositorio.getHipotesisByProyectoArea(String.valueOf(pYa.getId()));
			pYa.setHipotesis(hipotesis);
		}
		return proyectoAreas;
	}

	@Override
	public HipotesisDto getHipotesisById(String idHipotesis) {
		
		Hipotesis hipotesis = lienzoRepositorio.getHipotesisById(idHipotesis);
		HipotesisDto hipotesisDto = mapperDtoToBean.convertHipotesisToHipotesisDto(hipotesis);
		List<ProyectoArea> proyectoAreas = lienzoRepositorio.getProyectoAreaByHipotesis(idHipotesis);
		hipotesisDto.setProyectoAreas(proyectoAreas);
		return hipotesisDto;
	}

	@Override
	public String updateHipotesis(HipotesisDto hipotesisDto) {
		// TODO Auto-generated method stub
		return lienzoRepositorio.updateHipotesis(hipotesisDto);
	}

	@Override
	public HipotesisArbol arbolHipotesisByHipotesis(String idHipotesis) {
		// TODO Auto-generated method stub
		HipotesisArbol hipotesisInicial = new HipotesisArbol();
		hipotesisInicial.setId(Integer.valueOf(idHipotesis));
		
		return null;
	}

	@Override
	public void updateEstado(int idHipotesis, int estado) {
		// TODO Auto-generated method stub
		lienzoRepositorio.updateEstado(idHipotesis, estado);
	}

	@Override
	public List<Hipotesis> getHipotesisByProyecto(String idProyecto) {
		return lienzoRepositorio.getHipotesisByProyecto(idProyecto);
	}

	@Override
	public void fillNodoHipotesis(NodoArbol nodoArbol) {
		// TODO Auto-generated method stub
		lienzoRepositorio.fillNodoHipotesis(nodoArbol);
	}

	@Override
	public List<Relacion> getRelaciones(String idProyecto) {
		// TODO Auto-generated method stub
		return lienzoRepositorio.getRelaciones(idProyecto);
	}

	@Override
	public void insertarRelacion(int idHipotesisPadre, int idHijo, int idProyecto) {
		// TODO Auto-generated method stub
		lienzoRepositorio.insertarRelacion(idHipotesisPadre, idHijo, idProyecto);
	}

}
