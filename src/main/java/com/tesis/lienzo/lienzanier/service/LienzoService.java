package com.tesis.lienzo.lienzanier.service;

import java.util.List;

import com.tesis.lienzo.lienzanier.bean.Hipotesis;
import com.tesis.lienzo.lienzanier.bean.HipotesisArbol;
import com.tesis.lienzo.lienzanier.bean.NodoArbol;
import com.tesis.lienzo.lienzanier.bean.ProyectoArea;
import com.tesis.lienzo.lienzanier.bean.Relacion;
import com.tesis.lienzo.lienzanier.dto.HipotesisDto;

public interface LienzoService {
	
	public String save(HipotesisDto hipotesisDto);
	public List<ProyectoArea> getByProyecto(String idProyecto);
	public HipotesisDto getHipotesisById(String idHipotesis);
	public String updateHipotesis(HipotesisDto hipotesisDto);
	public HipotesisArbol arbolHipotesisByHipotesis(String idHipotesis);
	public void updateEstado(int idHipotesis, int estado);
	public List<Hipotesis> getHipotesisByProyecto(String idProyecto);
	public void fillNodoHipotesis(NodoArbol nodoArbol);
	public List<Relacion> getRelaciones(String idProyecto);
	public void insertarRelacion(int idHipotesisPadre, int id, int idProyecto);

}
