package com.tesis.lienzo.lienzanier.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tesis.lienzo.lienzanier.bean.Indicador;
import com.tesis.lienzo.lienzanier.repositorio.IndicadorRepositorio;
import com.tesis.lienzo.lienzanier.service.IndicadorService;

@Service
public class IndicadorServiceImpl implements IndicadorService{

	@Autowired
	IndicadorRepositorio indicadorRepositorio;

	@Override
	public void save(Indicador indicador) {
		// TODO Auto-generated method stub
		indicadorRepositorio.save(indicador);
	}

	@Override
	public List<Indicador> listar(String idProyecto) {
		// TODO Auto-generated method stub
		return indicadorRepositorio.listar(idProyecto);
	}

	@Override
	public Indicador getIndicador(String idIndicador) {
		// TODO Auto-generated method stub
		return indicadorRepositorio.getIndicador(idIndicador);
	}

	@Override
	public void saveMediciones(Indicador indicador) {
		// TODO Auto-generated method stub
		indicadorRepositorio.deleteFechaAndMediciones(indicador);
		indicadorRepositorio.saveMediciones(indicador);
	}
}
