package com.tesis.lienzo.lienzanier.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tesis.lienzo.lienzanier.bean.Proyecto;
import com.tesis.lienzo.lienzanier.service.ProyectoService;
import com.tesis.lienzo.lienzanier.repositorio.ProyectoRepositorio;

@Service
public class ProyectoServiceImpl implements ProyectoService{

	@Autowired
	ProyectoRepositorio proyectoRepositorio;
	
	@Override
	public List<Proyecto> getAll() {
		// TODO Auto-generated method stub
		return proyectoRepositorio.getAll();
	}

	@Override
	public String save(Proyecto proyecto) {
		// TODO Auto-generated method stub
		return proyectoRepositorio.save(proyecto);
	}

	@Override
	public List<Proyecto> getByUser(String id) {
		int idUsuario = Integer.parseInt(id);
		return proyectoRepositorio.getByUser(idUsuario);
	}

	@Override
	public void deleteById(String id) {
		// TODO Auto-generated method stub
		int idProyecto = Integer.parseInt(id);
		proyectoRepositorio.deleteById(idProyecto);
	}

	@Override
	public void update(Proyecto proyecto) {
		proyectoRepositorio.update(proyecto);
		
	}

	@Override
	public Proyecto getById(String id) {
		// TODO Auto-generated method stub
		return proyectoRepositorio.getById(id);
	}

}
