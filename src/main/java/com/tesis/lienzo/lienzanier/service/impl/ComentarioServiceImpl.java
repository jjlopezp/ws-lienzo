package com.tesis.lienzo.lienzanier.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tesis.lienzo.lienzanier.bean.Comentario;
import com.tesis.lienzo.lienzanier.repositorio.ComentarioRepository;
import com.tesis.lienzo.lienzanier.service.ComentarioService;

@Service
public class ComentarioServiceImpl implements ComentarioService{

	
	@Autowired
	ComentarioRepository comentarioRepository;

	@Override
	public List<Comentario> getComentariosByProyecto(String idProyecto) {
		// TODO Auto-generated method stub
		return comentarioRepository.getComentariosByProyecto(idProyecto);
	}

	@Override
	public void updateEstadoComentario(String idComentario, String estado) {
		// TODO Auto-generated method stub
		comentarioRepository.updateEstadoComentario(idComentario, estado);
	}

	@Override
	public void saveComentarioProyecto(Comentario comentario) {
		// TODO Auto-generated method stub
		comentarioRepository.saveComentarioProyecto(comentario);
	}

	@Override
	public List<Comentario> getComentariosByExperimento(String idExperimento) {
		// TODO Auto-generated method stub
		return comentarioRepository.getComentariosByExperimento(idExperimento);
	}

	@Override
	public void saveComentarioExperimento(Comentario comentario) {
		// TODO Auto-generated method stub
		comentarioRepository.saveComentarioExperimento(comentario);
	}

	@Override
	public void updateEstadoComentarioExperimento(String idComentario, String estado) {
		// TODO Auto-generated method stub
		comentarioRepository.updateEstadoComentarioExperimento(idComentario, estado);
	}

	@Override
	public List<Comentario> getComentariosDetalleByProyecto(String idProyecto) {
		// TODO Auto-generated method stub
		return comentarioRepository.getComentariosDetalleByProyecto(idProyecto);
	}
}
