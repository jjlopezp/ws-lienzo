package com.tesis.lienzo.lienzanier.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tesis.lienzo.lienzanier.bean.Experimento;
import com.tesis.lienzo.lienzanier.bean.Hipotesis;
import com.tesis.lienzo.lienzanier.bean.ProyectoArea;
import com.tesis.lienzo.lienzanier.bean.PruebaHipotesis;
import com.tesis.lienzo.lienzanier.dto.ExperimentoDto;
import com.tesis.lienzo.lienzanier.repositorio.ExperimentoRepositorio;
import com.tesis.lienzo.lienzanier.repositorio.LienzoRepositorio;
import com.tesis.lienzo.lienzanier.service.ExperimentoService;
import com.tesis.lienzo.lienzanier.util.Constantes;

@Service
public class ExperimentoServiceImpl implements ExperimentoService{

	@Autowired
	ExperimentoRepositorio experimentoRepositorio;
	
	@Autowired
	LienzoRepositorio lienzoRepositorio;
	
	@Override
	@Transactional
	public Integer save(ExperimentoDto experimentoDto) {
		// TODO Auto-generated method stub
		lienzoRepositorio.updateEstado(experimentoDto.getIdHipotesis(), Constantes.ESTADO_HIPOTESIS_EN_CURSO_NUEVO);
		return experimentoRepositorio.save(experimentoDto);
	}

	@Override
	public List<PruebaHipotesis> getTipoPruebas() {
		// TODO Auto-generated method stub
		return experimentoRepositorio.getTipoPruebas();
	}

	@Override
	public List<Experimento> getByProjecto(String idProyecto) {
		
		return experimentoRepositorio.getByProjecto(idProyecto);
	}

	@Override
	public void updateEstado(String idExperimento, String estado) {
		// TODO Auto-generated method stub
		int idExperi = Integer.parseInt(idExperimento);
		int estado2 = Integer.parseInt(estado);
		experimentoRepositorio.updateEstado(idExperi, estado2);
	}

	@Override
	public Experimento getDetalleExperimento(String idExperimento) {
		// TODO Auto-generated method stub
		return experimentoRepositorio.getDetalleExperimento(idExperimento);
	}

	@Override
	public List<ProyectoArea> getByProyectoAndEstado(String idProyecto, String estado) {
		
		//Obtener lista de ProyectoArea
		List<ProyectoArea> proyectoAreas = experimentoRepositorio.getAreaByProyecto(idProyecto);
		//int estadoInt = String.valueOf(estado);
		for(ProyectoArea pYa: proyectoAreas) {
			List<Experimento> hipotesis= new ArrayList<>();
			if(estado=="-1")
				hipotesis = experimentoRepositorio.getExperimentoByProyectoAreaTodos(String.valueOf(pYa.getId()));
			else
				hipotesis = experimentoRepositorio.getExperimentoByProyectoArea(String.valueOf(pYa.getId()), estado);
			pYa.setExperimentos(hipotesis);
		}
		return proyectoAreas;
	}

	@Override
	public void saveMediciones(Experimento experimento) {
		// TODO Auto-generated method stub
		experimentoRepositorio.deleteFechaAndMediciones(experimento);
		experimentoRepositorio.saveMediciones(experimento);
	}

	@Override
	@Transactional
	public void finalizarExperimento(String idHipotesis, String idExperimento) {
		// TODO Auto-generated method stub
		experimentoRepositorio.updateEstado(Integer.parseInt(idExperimento), Constantes.ESTADO_EXPERIMENTO_FINALIZADO);
		lienzoRepositorio.updateEstado(Integer.parseInt(idHipotesis), Constantes.ESTADO_HIPOTESIS_FINALIZADO);
		
	}

	@Override
	public void guardarCambiosExperimento(ExperimentoDto experimentoDto) {
		// TODO Auto-generated method stub
		experimentoRepositorio.guardarCambiosExperimento(experimentoDto);
	}


	

}
