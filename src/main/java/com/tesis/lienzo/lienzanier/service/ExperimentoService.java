package com.tesis.lienzo.lienzanier.service;


import com.tesis.lienzo.lienzanier.dto.ExperimentoDto;

import java.util.List;

import com.tesis.lienzo.lienzanier.bean.Experimento;
import com.tesis.lienzo.lienzanier.bean.ProyectoArea;
import com.tesis.lienzo.lienzanier.bean.PruebaHipotesis;

public interface ExperimentoService {

	Integer save(ExperimentoDto experimentoDto);

	List<PruebaHipotesis> getTipoPruebas();

	List<Experimento> getByProjecto(String idProyecto);

	void updateEstado(String idExperimento, String estado);

	Experimento getDetalleExperimento(String idExperimento);

	List<ProyectoArea> getByProyectoAndEstado(String idProyecto, String estado);

	void saveMediciones(Experimento experimento);

	void finalizarExperimento(String idHipotesis, String idExperimento);

	void guardarCambiosExperimento(ExperimentoDto experimentoDto);


	

}
