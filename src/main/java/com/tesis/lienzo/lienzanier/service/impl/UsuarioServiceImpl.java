package com.tesis.lienzo.lienzanier.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tesis.lienzo.lienzanier.repositorio.UsuarioRepositorio;
import com.tesis.lienzo.lienzanier.service.UsuarioService;

import com.tesis.lienzo.lienzanier.bean.Usuario;

@Service
public class UsuarioServiceImpl implements UsuarioService{

	@Autowired
	UsuarioRepositorio usuarioRepositorio;
	
	@Override
	public List<Usuario> getAllUsuarios() {
		// TODO Auto-generated method stub
		return usuarioRepositorio.findAll();
	}

	@Override
	public String save(Usuario usuario) {
		return usuarioRepositorio.save(usuario);
	}

	@Override
	public Usuario login(Usuario usuario) {
		// TODO Auto-generated method stub
		return usuarioRepositorio.login(usuario);
	}

	@Override
	public List<Usuario> findByEmail(String email, String idUser) {
		return usuarioRepositorio.findByEmail(email, Integer.valueOf(idUser));
	}

	@Override
	public Usuario loginG(Usuario usuario) {
		// TODO Auto-generated method stub
		return usuarioRepositorio.loginG(usuario);
	}

	@Override
	public boolean validateUsuarioRegistrado(String usuario) {
		boolean result = usuarioRepositorio.validateUsuarioRegistrado(usuario);
		return result;
	}

	@Override
	public boolean validateEmailRegistrado(String usuario) {
		boolean result = usuarioRepositorio.validateEmailRegistrado(usuario);
		return result;
	}

	@Override
	public List<Usuario> getByIdProyecto(String idProyecto) {
		// TODO Auto-generated method stub
		return usuarioRepositorio.getByIdProyecto(idProyecto);
	}

	@Override
	public Usuario registerShare(Usuario usuario) {
		// TODO Auto-generated method stub
		return usuarioRepositorio.registerShare(usuario);
	}

}
