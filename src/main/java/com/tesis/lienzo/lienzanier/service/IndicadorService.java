package com.tesis.lienzo.lienzanier.service;


import java.util.List;

import com.tesis.lienzo.lienzanier.bean.Indicador;

public interface IndicadorService {

	void save(Indicador indicador);

	List<Indicador> listar(String idProyecto);

	Indicador getIndicador(String idIndicador);

	void saveMediciones(Indicador indicador);

	

}
