package com.tesis.lienzo.lienzanier.service;


import java.util.List;

import com.tesis.lienzo.lienzanier.bean.Metrica;

public interface MetricaService {

	public List<Metrica> getMetricasByIdProyecto(String idProyecto);

	public int save(Metrica proyecto);

	public String update(Metrica proyecto);

	public void delete(String id);
}
