package com.tesis.lienzo.lienzanier.dto;

import java.io.Serializable;
import java.util.List;

import com.tesis.lienzo.lienzanier.bean.Condicion;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExperimentoDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private int idHipotesis;
	private int idTipoPrueba;
	private int idProyecto;
	private int estado;
	private String descripcion;
	private List<Integer> idUsuarios;
	private List<Condicion> condiciones;
	private List<Integer> metricas;

}
