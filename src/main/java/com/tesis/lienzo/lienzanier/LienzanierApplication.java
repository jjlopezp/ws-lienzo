package com.tesis.lienzo.lienzanier;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(exclude = {
})
public class LienzanierApplication {

	public static void main(String[] args) {
		SpringApplication.run(LienzanierApplication.class, args);
	}
}
